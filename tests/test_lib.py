# Copyright 2020 info@un8s.com
# License: MIT, see LICENSE for details.

import enum
import unittest
import sys
sys.path.insert(0, '/home/lc/lightcapsule-dev')
import time

import fakeredis

from lib import timestamp, uuid, sys_info, CircuitBreaker
from lib.mbus import Event, MessageBus, UndeliveredError, BrokerError

class TestFunctions(unittest.TestCase):

    def test_timestamp(self):
        self.assertIsInstance(timestamp(), int)
        t1 = timestamp()
        t2 = timestamp()
        self.assertGreater(t2, t1)

    def test_uuid(self):
        self.assertIsInstance(uuid(), str)
        u1 = uuid()
        u2 = uuid()
        self.assertNotEqual(u1, u2)

    def test_sys_info(self):
        si = sys_info()
        self.assertTrue(si['sys.serial'] != '')
        self.assertGreater(timestamp(), si['sys.boot_time'])


class TestCircuitBreaker(unittest.TestCase):

    def test_functions(self):
        cb = CircuitBreaker('TestBreaker', 3, 3)
        self.assertTrue(cb.is_closed())

        cb.test()
        cb.test()
        self.assertTrue(cb.is_closed())

        cb.test()   # trigger open
        cb.test()
        self.assertFalse(cb.is_closed())
        time.sleep(0.1)
        self.assertGreater(cb.recovery_time, 0.0)

        time.sleep(4.0)
        self.assertTrue(cb.is_closed()) # retry #1
        self.assertEqual(cb.recovery_time, 0.0)

        cb.test()
        self.assertTrue(cb.is_closed()) # retry #2

        cb.reset()
        self.assertTrue(cb.is_closed())


class TestEvent(unittest.TestCase):

    def test_methods(self):
        e = Event('t1', timestamp(), 'host1', [], {})
        self.assertEqual(e.topic, 't1')
        self.assertEqual(e.host, 'host1')

        self.assertGreater(timestamp(), e.timestamp)
        ts_m = e.iso_timestamp()
        self.assertEqual(len(ts_m), 16)
        ts_s = e.iso_timestamp(timespec='seconds')
        self.assertEqual(len(ts_s), 19)
        ts_mu = e.iso_timestamp(timespec='micro')
        self.assertEqual(len(ts_mu), 23)

        self.assertEqual(e.get('unknown', 'default'), 'default')
        self.assertNotIn('unknown', e)
        with self.assertRaises(KeyError):
            e['unknown']

        self.assertEqual(e.tags(), [])
        self.assertEqual(e.tags('t1'), [])

        self.assertTrue(e.matches())
        self.assertFalse(e.matches('t1'))
        self.assertFalse(e.matches('t[0-9]'))

        e = Event('t1', timestamp(), 'host1', ['t1'], {'k1': 'v1'})
        self.assertEqual(e.get('unknown', 'default'), 'default')
        self.assertEqual(e.get('k1', 'default'), 'v1')
        self.assertEqual(e['k1'], 'v1')
        self.assertIn('k1', e)

        self.assertEqual(e.tags(), ['t1'])
        self.assertEqual(e.tags('t1'), ['t1'])
        self.assertEqual(e.tags('t2'), [])

        self.assertTrue(e.matches())
        self.assertFalse(e.matches('tx'))
        self.assertFalse(e.matches('t[a-z]'))
        self.assertTrue(e.matches('t1'))
        self.assertTrue(e.matches('t[0-9]'))


class TestMessageBusBasics(unittest.TestCase):

    def test_topic_names(self):
        mb = MessageBus('test', broker=fakeredis.FakeRedis())

        self.assertEqual(mb._event_topic('abc'), 'test:topic:abc:event')
        self.assertEqual(mb._store_topic('*'), 'test:topic:*:store')

        self.assertEqual(mb._topic_name('test:topic:abc:event'), 'abc')
        self.assertEqual(mb._topic_name(b'test:topic:abc:event'), 'abc')
        with self.assertRaises(AssertionError):
            mb._topic_name('test:topic:abc')
        with self.assertRaises(AssertionError):
            mb._topic_name('xx:topic:abc:test')


class TestMessageBus(unittest.TestCase):
    
    def setUp(self):
        self._server = fakeredis.FakeServer()
        self._named_sender = MessageBus('test', 'named', broker=fakeredis.FakeRedis(server=self._server))
        self._anon_sender = MessageBus('test', broker=fakeredis.FakeRedis(server=self._server))

        self._subscriber = MessageBus('test', broker=fakeredis.FakeRedis(server=self._server))
        # Implicit subscriptions:
        self._subscriber.get_event('named')
        self._subscriber.get_event(['other', 'extra'])

        self.tearDown()

    def test_reconnect(self):
        self._named_sender.send_event({'k1': 'before reconnect'})
        e = self._subscriber.get_event('named')
        self.assertIsNotNone(e)

        self._server.connected = False
        with self.assertRaises(BrokerError):
            self._subscriber.get_event('named')

        self._server.connected = True
        self._subscriber.reconnect(broker=fakeredis.FakeRedis(server=self._server))
        self._subscriber.get_event('named')  # Implicit subscription

        self._named_sender.send_event({'k1': 'after reconnect'})
        e = self._subscriber.get_event('named')
        self.assertIsNotNone(e)

    def test_events_without_tags(self):
        self._named_sender.send_event({'k1': 'v1'})
        e = self._subscriber.get_event('named')
        self.assertIsNotNone(e)
        e = self._subscriber.get_event(['other', 'extra'])
        self.assertIsNone(e)

        self._named_sender.send_event({'k1': 'v1'}, 'other')
        e = self._subscriber.get_event('named')
        self.assertIsNotNone(e)
        e = self._subscriber.get_event(['other', 'extra'])
        self.assertIsNotNone(e)

        self.assertRaises(AssertionError, self._anon_sender.send_event, {'k1': 'v1'})

        self._anon_sender.send_event({'k1': 'v1'}, 'other')
        e = self._subscriber.get_event('named')
        self.assertIsNone(e)
        e = self._subscriber.get_event(['other', 'extra'])
        self.assertIsNotNone(e)

    def test_events_with_tags(self):
        self._named_sender.send_event({'k1': 'v1'})
        e = self._subscriber.get_event('named', tags=['t1'])
        self.assertIsNone(e)

        self._named_sender.send_event({'k1': 'v1'})
        e = self._subscriber.get_event('named', tags=['t.'])
        self.assertIsNone(e)

        self._named_sender.send_event({'k1': 'v1'}, tags=['t1'])
        e = self._subscriber.get_event('named', tags=['tx'])
        self.assertIsNone(e)

        self._named_sender.send_event({'k1': 'v1'}, tags=['t1'])
        e = self._subscriber.get_event('named', tags=['t1'])
        self.assertIsNotNone(e)

        self._named_sender.send_event({'k1': 'v1'}, tags=['t1'])
        e = self._subscriber.get_event('named', tags=['t[a-z]'])
        self.assertIsNone(e)

        self._named_sender.send_event({'k1': 'v1'}, tags=['t1'])
        e = self._subscriber.get_event('named', tags=['t[0-9]'])
        self.assertIsNotNone(e)

    def test_stored_events(self):
        events = self._named_sender.read_events()
        self.assertEqual(len(events), 0)

        self._named_sender.send_event({'k1': 'v1'}, persist=True)
        self._named_sender.send_event({'k1': 'v1'}, tags=['t1'], persist=True)

        events = self._named_sender.read_events()
        self.assertEqual(len(events), 2)

        first = events[0].timestamp
        last = events[-1].timestamp

        events = self._named_sender.read_events(n=1)
        self.assertEqual(len(events), 1)
        self.assertEqual(events[0].timestamp, first)

        events = self._named_sender.read_events(n=-1)
        self.assertEqual(len(events), 1)
        self.assertEqual(events[0].timestamp, last)

        events = self._named_sender.read_events(tags=['tx'])
        self.assertEqual(len(events), 0)

        events = self._named_sender.read_events(tags=['t1', 't2'])
        self.assertEqual(len(events), 1)

        events = self._named_sender.read_events(tags=['t[a-z]'])
        self.assertEqual(len(events), 0)

        events = self._named_sender.read_events(tags=['t[0-9]'])
        self.assertEqual(len(events), 1)

        self.assertGreater(len(self._named_sender.stored_topics()), 0)
        self.assertEqual(self._named_sender.stored_topics('named'), ['named'])
        self.assertEqual(self._named_sender.stored_topics('name.*'), ['named'])

        # events = self._anon_sender.read_events('*', tags=['t[0-9]'])
        # self.assertEqual(len(events), 1)
        events = self._anon_sender.read_events(['named', 'extra'], tags=['t[0-9]'])
        self.assertEqual(len(events), 1)

    def test_request_response(self):
        self.assertIsNone(self._anon_sender.send_request({}, 'named', must_deliver=False))
        with self.assertRaises(UndeliveredError):
            self._anon_sender.send_request({}, 'named', must_deliver=True)

        self.assertIsNone(self._named_sender.get_request())
        self.assertIsNone(self._anon_sender.send_request({}, 'named', must_deliver=True))
        r = self._named_sender.get_request()
        self.assertEqual(r.topic, 'named')
        with self.assertRaises(UndeliveredError):
            self._named_sender.send_response({}, r, must_deliver=True)

    def test_store(self):
        # self._named_sender.store.delete('k1')
        self.assertIsNone(self._named_sender.store.get('k1'))
        
        self._named_sender.store.set('k1', 'v1')
        self.assertEqual(self._named_sender.store.get('k1'), 'v1')
        self.assertEqual(self._named_sender.store.keys(), ['k1'])

        self._named_sender.store.delete('k1')
        self.assertIsNone(self._named_sender.store.get('k1'))
        self.assertEqual(self._named_sender.store.keys(), [])

    def tearDown(self):
        self._named_sender.purge_events()
        self._anon_sender.purge_events('other')
        self._anon_sender.purge_events('extra')


if __name__ == '__main__':
    unittest.main()
