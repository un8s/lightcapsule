# Copyright 2020 info@un8s.com
# License: MIT, see LICENSE for details.

import enum
import unittest
import sys
sys.path.insert(0, '/home/lc/lightcapsule-dev')

from lib.srv import ServiceDef, ServiceError, ServiceAddress, Service


class TestServiceAddress(unittest.TestCase):

    def test_find(self):
        sa = ServiceAddress.find('_lc')
        self.assertEqual(sa.port, 6379)

        sa = ServiceAddress.find('_lc-bc')
        self.assertEqual(sa.user, 'lc')


class StartMode(enum.Enum):
    auto = 'auto'
    ondemand = 'ondemand'


def _check_check(x):
    if x > 0.0: raise ValueError(x)
    return x

class TestService(unittest.TestCase):

    def setUp(self):
        self.service_def = {
            'app': 'test',
            'name': 'disk',
            'version': '20.12',
            'commands': {
                'start': {
                    'args': {
                        'name': {
                            'type': str,
                            'required': True,
                            'choices': ['hd', 'bk']
                        },
                        'mode': {
                            'type': str,
                            'required': False,
                            'default': StartMode.ondemand.name,
                            'choices': list(StartMode.__members__)
                        },
                        'range': {
                            'type': int,
                            'range': (-3, 3),
                            'default': 0
                        },
                        'check': {
                            'type': float,
                            'check': _check_check
                        }
                    },
                    'call': None
                }
            }
        }

    def test_command_from_dict(self):

        def start_method(name, mode):
            return f'start({name}, {mode.value})'

        self.service_def['commands']['start']['call'] = start_method
        sd = ServiceDef(self.service_def)

        self.assertEqual(sd.name, 'disk')

        with self.assertRaises(ServiceError):
            sd.command_from_dict({})

        with self.assertRaises(ServiceError):
            sd.command_from_dict({'command': 'unknown'})

        with self.assertRaises(ServiceError):
            sd.command_from_dict({'command': 'start'})

        with self.assertRaises(ValueError):
            sd.command_from_dict({'command': 'start', 'name': 12})

        with self.assertRaises(ValueError):
            sd.command_from_dict({'command': 'start', 'name': 'wrong'})

        with self.assertRaises(ValueError):
            sd.command_from_dict({'command': 'start', 'name': 'hd', 'range': 4})

        with self.assertRaises(ValueError):
            sd.command_from_dict({'command': 'start', 'name': 'hd', 'check': 0.1})

        self.assertEqual(sd.command_from_dict({'command': 'start', 'name': 'hd'}), (start_method, {'name': 'hd', 'mode': StartMode.ondemand.value, 'range': 0, 'check': None}))

    def test_subclass(self):

        self.service_def['commands']['start']['call'] = 'start_method'

        class TestService(Service):

            def start_method(self):
                return f'start({name}, {mode.value})'

        s = TestService(self.service_def)

        self.assertEqual(s._sd.command_from_dict({'command': 'start', 'name': 'hd'}), ('start_method', {'name': 'hd', 'mode': StartMode.ondemand.value, 'range': 0, 'check': None}))


if __name__ == '__main__':
    unittest.main()
