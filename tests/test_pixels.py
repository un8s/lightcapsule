# Copyright 2020 info@un8s.com
# License: MIT, see LICENSE for details.

import unittest
import sys
sys.path.insert(0, '/home/lc/lightcapsule-dev')
import time

from lib.pixels import Pixels


def get_pixels(size, overlays=1):
    if Pixels.has_pwm:
        return Pixels('D18', size, overlays)

    if Pixels.has_spi:
        return Pixels((0,0), size, overlays)
    
    assert False, 'No supported hardware'


class TestWithHardware(unittest.TestCase):

    def test_set_row(self):
        p = get_pixels((1, 8))
        p.brightness = 1
        p.set(1, (1, 8), (255, 255, 255))
        time.sleep(1.0)

    def test_set_matrix(self):
        p = get_pixels((8, 8))
        p.brightness = 1
        p.set(1, (1, 8), (255, 255, 255))
        p.set(1, (8, 1), (255, 255, 255))
        time.sleep(1.0)

    def test_overlay_matrix(self):
        p = get_pixels((8, 8), overlays=3)
        p.brightness = 1
        
        p.set(1, [(4, 4), (4, 5), (5, 4), (5, 5)], 'white')
        p.set(2, [(4, 4), (5, 5)], (255, 0, 0))
        p.set(3, [(5, 5)], (0, 255, 0))
        time.sleep(1.0)

        p.clear(2)
        time.sleep(1.0)

        p.set(3, [(5, 5)])
        time.sleep(1.0)

    def test_update_matrix(self):
        p = get_pixels((8, 8), overlays=3)
        p.brightness = 1

        p.fill(1, 'red', update=False)
        time.sleep(0.5)
        p.set(1, [(4, 4), (4, 5), (5, 4), (5, 5)], 'white', update=False)
        p.update()
        time.sleep(1)

        p.clear()

    def test_brightness(self):
        p = get_pixels((8, 8))
        p.brightness = 20
        p.fill(1, 'violet', update=False)

        for bx in range(20, 0, -1):
            p.update(bx)
            time.sleep(0.05)

    def test_colors(self):
        p = get_pixels((8, 8))
        p.brightness = 20
        p.fill(1, 'red')
        time.sleep(0.5)
        p.fill(1, 'green')
        time.sleep(0.5)
        p.fill(1, 'blue')
        time.sleep(0.5)
        p.clear()


if __name__ == '__main__':
    unittest.main()
