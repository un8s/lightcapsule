# Copyright 2020 info@un8s.com
# License: MIT, see LICENSE for details.

import collections as _collections
import datetime as _datetime
import os as _os
import signal as _signal
import socket as _socket
import sys as _sys
import threading as _threading
import time as _time

import zeroconf as _zeroconf

from lib import log, AttrDict, CircuitBreaker
from lib.mbus import MessageBus, BrokerError


__all__ = ['ServiceError', 'ServiceAddress', 'ServiceDef', 'Service']


class ServiceError(Exception): ...


class ServiceAddress(AttrDict):
    """Service address as discovered via ZeroConf."""

    @classmethod
    def find(cls, type_, timeout=2.0):
        """Return ServiceAddress for service type, or None."""
        zbr = None
        services = []

        class BrListener:
            def add_service(self, zc, type, name):
                services.append(zc.get_service_info(type, name))

        zbr = _zeroconf.ServiceBrowser(_zeroconf.Zeroconf(), f'{type_}._tcp.local.', BrListener())
        start = _time.time()
        while len(services) == 0 and _time.time()-start < timeout:
            _time.sleep(0.1)
        zbr.cancel()
        log.debug(services)

        if len(services) == 0:
            return None

        assert len(services) == 1, services
        s = services[0]
        return cls(
            s.server,
            [_socket.inet_ntoa(addr) for addr in ([s.address] if hasattr(s, 'address') else s.addresses)],
            s.port,
            {k.decode(encoding='utf-8'): v.decode(encoding='utf-8') for k, v in s.properties.items()}
        )

    def __init__(self, hostname, addresses, port, properties):
        assert isinstance(hostname, str) and len(hostname), hostname
        assert isinstance(addresses, list) and len(addresses) and isinstance(addresses[0], str) and len(addresses[0]), addresses
        assert isinstance(port, int), port
        assert isinstance(properties, dict), properties
        super().__init__(properties)
        self._hostname = hostname
        self._addresses = addresses
        self._port = port

    @property
    def hostname(self):
        return self._hostname

    @property
    def address(self):
        return self._addresses[0]

    @property
    def port(self):
        return self._port    

    def properties(self):
        return list(self.keys())

    def __str__(self):
        return f'ServiceAddress({self.hostname}, {self.address}:{self.port}, {self._d})'


class ServiceDef(AttrDict):
    """Service definition.

    General:    
        'app': <str:app name> (mandatory; used to group services that belong together and communicate via the same message bus)
        'name': <str:service name> (mandatory)
        'version': <str:service version> (mandatory; must be greater than mbus version)
        'help': <str> (optional)
        'checkuid': <int:uid> (optional; service will fail if not started with this user id)

    Service functionality:
        'start': <str or callable:service method or function that will be called by _start> (optional)
        'run': <str or callable:service method or function that will be called by _run> (optional)
        'stop': <str or callable:service method or function that will be called by _stop> (optional)

    Message Bus:
        'mbus': {
            'broker.name': <str:name of message bus to be resolved with ZeroConf> (optional)
            'broker.host': <str:hostname of message bus broker> (optional; default: localhost)
            'broker.port': <int:port of message bus broker> (optionall; default: 6379)
        } (optional; default: broker.host='localhost', broker.port=6379)

    Configuration:
        Service configuration parameters that are persisted on the message bus.

        'conf': {
            '<parameter name>': {
                'help': <str> (optional)
                'value': <any:initial/default value> (mandatory)
                'type': <type or callable that raises TypeError if type is not correct> (optional; default: None)
                'check': <callable that returns sanitized value or raises ValueError> (optional; default: None)
                'required': <bool:True if parameter value can be None> (optional; default: False)
                'readonly': <bool:True if parameter should not be modified> (optional; default: False)
                'internal': <bool:True if parameter is flagged internal> (optional; default: False)
            }    
        } {optional}

    Schedules:
        Periodically called service methods or functions. Called in separate thread. It is guaranteed that a schedule is not called
        while a previous call is still running. The order of schedule definitions matters during service
        start (when init is True): schedules will be called in the defined order.

        'schedules': {
            '<schedule name>': {
                'help': <str> (optional)
                'call': <str or callable:service method or function to be called by scheduler> (optional; default: <schedule name>)
                'init': <bool:also execute once during start of the service> (optional; default: False)
                'interval': <int or float:interval in minutes (int) or seconds (float with 0.01=1 second)> (either 'interval' or 'minute' is required)
                'minute': <int:minute at which call will be scheduled once every hour> (either 'interval' or 'minute' is required)
            }
        } (optional)

    Service API: Commands
        The service will call a service method or function when a defined command is received. If 'call'
        is not defined for a command, a service method named 'do_<command name>' is searched. Either is called
        with all 'args' as keyword arguments. If 'call' is not defined and no service method 'do_<command name>'
        is found, _do_command is called with the <command name> as first argument and all 'args' as dictionary.

        'commands': {
            '<command name>': {
                'help': <str> (optional)
                'call': <str or callable:service method or function to be called> (optional; default: <command name> or _do_command)
                'args': <str:name of other command with same args definition> (optional)
                'args': {
                    '<argument name>': {
                        'type': <any:type to check argument value against> (optional; default: no type check)
                        'required': <bool:True if argument is mandatory> (optional; default: False)
                        'default': <any:value for unspecified argument when required is False> (optional; default: None)
                        'choices': <[any]:list of allowed values> (optional)
                        'range': (<any:minimum inclusive value>,<any:maximum inclusive value>) (optional)
                        'check': <callable that returns sanitized value or raises ValueError> (optional)
                    }
                } (optional)
            }
        } (optional)

    Service API: Consumed Events
        The service method or function will be called with the event as first argument.

        'events': {
            'consume': {
                <consumer name>: {
                    'source': <str or ServiceDef:name or definition of emitting service> (mandatory)
                    'filter': <str or [str]: pattern or list of patterns to filter events> (optional)
                    'call': <str or callable:service method or function to call for received events> (optional; default: <consumer name>)
                }
            } (optional)
        } (optional)

    Service API: Emitted Events
        Events that the service emits.

        'events': {
            'emit': {
                '<event pattern>': {
                    'help': <str> (optional)
                    'args': {
                        '<argument name>': {
                            'type': <any>
                        }
                    } (optional)
                }
            } (optional)
        } (optional)

    """

    def __init__(self, service_def):
        assert isinstance(service_def, dict) and 'name' in service_def and 'version' in service_def, service_def
        super().__init__(service_def)
        for k in ('app', 'name', 'version'):
            assert k in self._d, (k, self._d)
        
        if 'mbus' in self._d:
            self._d['mbus'] = AttrDict(self._d['mbus'])

        # Add implicit commands.
        if 'commands' not in self._d:
            self._d['commands'] = {}
        self.commands['_info'] = { 'call': '_do_info', 'help': 'Return info about service.' }
        self.commands['_help'] = { 'call': '_do_help', 'help': 'Return service definition.' }

    def conf_def(self, param_name):
        """Return conf parameter definition."""
        return dict(self._d['conf'][param_name])

    def info(self):
        """Return info about service."""
        return {
            'app': self.app,
            'name': self.name,
            'version': self.version
        }

    def help(self):
        """Return service definition."""
        return dict.copy(self._d)

    def command_from_dict(self, cd):
        """Return (str|callable, args) for command input as dict. Raise ServiceError, ValueError."""
        assert isinstance(cd, dict), cd
        if 'commands' not in self:
            raise ServiceError('Service does not accept commands')

        if 'command' not in cd:
            raise ServiceError(f'Missing command: {cd}')

        if cd['command'] not in self.commands:
            raise ServiceError(f'Unknown command: {cd["command"]}')

        command_def = self.commands[cd['command']]
        args_defs = command_def.get('args', {})
        assert isinstance(args_defs, (str, dict)), command_def
        if isinstance(args_defs, str):  # Take args from specified command.
            args_defs = self.commands[args_defs]['args']
        args = {}
        for arg_name, arg_def in args_defs.items():
            arg_value = cd.get(arg_name, None)
            if arg_value is None:
                if arg_def.get('required', False):
                    raise ServiceError(f'Missing required argument: {arg_name}')
                arg_value = arg_def.get('default')
            else:
                if 'type' in arg_def and not isinstance(arg_value, arg_def['type']):
                    raise ValueError(f'Incorrect type for argument: {arg_name}')
                if 'choices' in arg_def and arg_value not in arg_def['choices']:
                    raise ValueError(f'Invalid choice for argument: {arg_name}')
                if 'range' in arg_def and not (arg_def['range'][0] <= arg_value <= arg_def['range'][1]):
                    raise ValueError(f'Argument not within range: {arg_name}')
                if 'check' in arg_def:
                    arg_value = arg_def['check'](arg_value)

            args[arg_name] = arg_value

        return command_def.get('call'), args


class _ConfParam:
    """Configuration parameter."""

    _initialized = False

    def __init__(self, key, value, type=None, *, check=None, required=False, readonly=False, internal=False):
        self.key = key
        self.default_value = self.value = value
        self.type = type
        self._check = check
        self.required = required
        self.readonly = readonly
        self.internal = internal
        self._initialized = True

    def check(self, value):
        """Return value after checking its validity. Raise ValueError."""
        return value if self._check is None else self._check(value)

    def __setattr__(self, name, value):
        if not self._initialized:
            super().__setattr__(name, value)
            return
            
        assert name == 'value', f'Cannot modify {name} after initialization'

        # Read-only check.
        if self.readonly:
            raise ValueError(f"Value for '{self.key}' cannot be set.")

        if value != self.value:
            # 'None' check.
            if value is None and self.required:
                raise ValueError(f"Value for '{self.key}' cannot be 'None'.")

            # Type check.
            if value is not None and self.type is not None:
                if callable(self.type):
                    if not self.type(value):
                        raise TypeError(f"Value for '{self.key}' did not pass type check: {value}")
                else:
                    if not isinstance(value, self.type):
                        raise TypeError(f"Value for '{self.key}' must be of type '{self.type}': {value}")

            super().__setattr__(name, None if value is None else self.check(value))

    def as_dict(self):
        return {'key': self.key, 'value': self.value, 'required': self.required, 'readonly': self.readonly, 'internal': self.internal}


class ServiceConf:
    """Service configuration."""

    def __init__(self, service_conf, srv):
        self._param = {}
        self._srv = srv
        if service_conf is not None:
            assert isinstance(service_conf, dict), service_conf
            for param_name, param_def in service_conf.items():
                self._param[param_name] = _ConfParam(param_name, **param_def)
                cv = self._srv.read_value(param_name)
                if cv is not None:
                    self._param[param_name].value = cv
                log.debug(f'Using {param_name} = {self._param[param_name].value}')

    def set(self, key, value):
        new_value = value if value is not None else self._param[key].default_value
        old_value = self._param[key].value

        if new_value != old_value:
            self._param[key].value = new_value
            self._srv.store_value(key, self._param[key].value)

    def get(self, key):
        return self._param[key].value

    def __getitem__(self, key):
        return self.get(key)

    def list(self, pattern=None):
        return [self._param[k].as_dict() for k, p in self._param.items() if not p.internal and (pattern is None or _re.fullmatch(pattern, k))]


class Service:

    def __init__(self, service_def):
        self._sd = service_def if isinstance(service_def, ServiceDef) else ServiceDef(service_def)
        assert MessageBus.version <= self._sd.version
        self._def_sched_timer = {}
        self._mbus = None
        self._mbus_breaker = CircuitBreaker()
        self._command_listener, self._event_listener = None, None

    def emit_event(self, name, data=None, *, alert=False, log=False):
        """"Send named event on message bus. If alert is True, add tag 'alert.<name>'.
        If log is True, add tag 'log.<name>' and also persist the event."""
        # TODO Check if event data matches service info.
        tags, persist = [name], False
        if alert:
            tags.append(f'alert.{name}')
        if log:
            tags.append(f'log.{name}')
            persist = True
        self._mbus.send_event(data or {}, tags=tags, persist=persist)

    def read_log(self, n=None, reverse=False):
        """Return list of logged events."""
        return self._mbus.read_events('*', tags=['log\..*'], n=n, reverse=reverse)

    def send_command(self, dest, command, timeout=10.0, **kwargs):
        """Return reply or None adter sending command to dest service.
        Wait until timeout for reply. Raise UndeliveredError."""
        assert isinstance(dest, (str, ServiceDef)), dest
        if isinstance(dest, ServiceDef):
            dest = dest.name
        return self._mbus.send_request({'command': command, **kwargs}, dest, must_deliver=True, timeout=timeout)

    def store_value(self, key, value):
        """Store value on message bus."""
        # TODO Define stored values in service info.
        self._mbus.store.set(key, value)

    def read_value(self, key):
        """Return value from message bus."""
        return self._mbus.store.get(key)

    def delete_value(self, key):
        """Delete value from message bus."""
        self._mbus.store.delete(key)

    def find_value(self, pattern=None):
        """Return list of names for all currently stored values."""
        return self._mbus.store.keys(pattern)

    def error_response(self, error, **kwargs):
        """Return dict with error response."""
        return {'error': error, **kwargs}

    def response(self, **kwargs):
        """Return dict with response."""
        return {**kwargs}

    def _call(self, _name, *args, **kwargs):
        """Return result after calling named callable."""
        assert callable(_name) or isinstance(_name, str), _name
        if isinstance(_name, str):
            _name = getattr(self, _name)
            assert callable(_name), _name
        return _name(*args, **kwargs)

    def _schedule(self, name, init=False):
        """Run and re-schedule scheduled methods."""
        assert name in self._sd.schedules, name
        sch_def = AttrDict(self._sd.schedules[name])

        if not init or (init and sch_def.get('init', False)):
            try:
                log.debug(f'Running schedule: {name}')
                self._call(sch_def.get('call', name))
            except Exception as ex:
                log.error(ex)

        # Re-schedule.
        if 'interval' in sch_def:
            if isinstance(sch_def.interval, int) and 0 < sch_def.interval:
                log.debug(f'Reschedule {name} in {sch_def.interval} minutes.')
                next_sec = sch_def.interval * 60.0
            elif isinstance(sch_def.interval, float) and 0.0 < sch_def.interval:
                next_sec = int(sch_def.interval * 100)
                log.debug(f'Reschedule {name} in {next_sec} seconds.')
            else:
                assert False, sch_def
            self._def_sched_timer[name] = _threading.Timer(next_sec, self._schedule, args=[name])
        elif 'minute' in sch_def:
            assert isinstance(sch_def.minute, int) and 0 <= sch_def.minute <= 59, sch_def
            dt = _datetime.datetime.now()
            next_min = 60 - dt.minute
            if next_min <= 0:
                next_min += 60
            next_min += sch_def.minute
            log.debug(f'Reschedule {name} in {next_min} minutes.')
            self._def_sched_timer[name] = _threading.Timer(next_min*60.0, self._schedule, args=[name])
        else:
            assert False, sch_def
        self._def_sched_timer[name].start()

    def schedule(self, when, what, *args, **kwargs):
        """Return Timer after scheduling a single call of a named callable."""
        assert isinstance(when, str) and len(when), when
        if when[0] == '+':
            log.debug(f'Schedule {what} in {int(when)} seconds.')
            t = _threading.Timer(int(when), self._call, [what, *args], kwargs)
            t.start()
            return t
        else:
            assert False, when

    def _do_info(self):
        """Return info about service."""
        return self.response(**self._sd.info())

    def _do_help(self):
        """Return service definition."""
        return self.response(**self._sd.help())

    def _do_command(self, command, args):
        """Return result of processed command."""
        assert isinstance(command, str), command
        assert isinstance(args, AttrDict), args
        return self.response()

    def _connect(self):
        """Re-/Connect to message bus."""
        broker_host, broker_port = 'localhost', 6379
        if 'mbus' in self._sd:
            if 'broker.name' in self._sd.mbus:
                name = self._sd.mbus['broker.name']
                while True:     # TODO Break after timeout.
                    sa = ServiceAddress.find(name)
                    if sa is not None:
                        log.debug(f'Found {name}: {sa}')
                        broker_host, broker_port = sa.hostname, sa.port
                        break
                    _time.sleep(60.0)
            else:
                broker_host = self._sd.mbus.get('broker.host', broker_host)
                broker_port = self._sd.mbus.get('broker.port', broker_port)
        if self._mbus is None:
            self._mbus = MessageBus(self._sd.app, self._sd.name, broker_host=broker_host, broker_port=broker_port)
        else:
            self._mbus.reconnect(broker_host, broker_port)
        log.debug(f'Connecting to message bus {self._sd.app}:{self._sd.name}@{broker_host}:{broker_port}')

    def _call_mbus(self, method, *args, **kwargs):
        while True:
            try:
                r = getattr(self._mbus, method)(*args, **kwargs)
                self._mbus_breaker.reset()
                return r
            except BrokerError:
                self._mbus_breaker.test()
                if self._mbus_breaker.is_closed():
                    _time.sleep(self._mbus_breaker.error_count*10.0)
                else:
                    # Try to re-connect.
                    self._connect()
                    self._mbus_breaker.reset()

    def _listen_for_commands(self):
        """Loop of request listener."""
        while True:
            # Sleep on the request queue.
            req = self._call_mbus('get_request', timeout=6.0)
            if req is not None:
                assert req.topic == self._sd.name, req
                try:
                    # TODO Optionally run command in separate thread.
                    call, args = self._sd.command_from_dict(req.data())
                    if call is not None:
                        rsp = self._call(call, **args)
                    elif hasattr(self, f'do_{req.command}'):
                        rsp = getattr(self, f'do_{req.command}')(**args)
                    else:
                        rsp = self._do_command(req.command, AttrDict(args))
                except Exception as ex:
                    log.error(ex)
                    rsp = self.error_response(ex)
                finally:
                    if rsp is not None:
                        try:
                            self._mbus.send_response(rsp, req)
                        except BrokerError:
                            pass

    def _listen_for_events(self):
        """Loop of event listener."""
        assert 'events' in self._sd and 'consume' in self._sd.events, self._sd
        consume_defs = dict(self._sd.events['consume'])
        topics = []
        for event_name, event_def in consume_defs.items():
            if not isinstance(event_def['source'], list):
                event_def['source'] = [event_def['source']]
            for i, s in enumerate(event_def['source']):
                if isinstance(s, ServiceDef):
                    event_def['source'][i] = s.name
                topics.append(event_def['source'][i])
        topics = list(set(topics))
        log.debug(f'Listening for events: {topics}')
        while True:
            # Sleep on the event queue.
            e = self._call_mbus('get_event', topics, timeout=60.0)
            if e is not None:
                for event_name, event_def in consume_defs.items():
                    if e.topic not in event_def['source']:
                        continue
                    if 'filter' in event_def:
                        assert isinstance(event_def['filter'], (str, list)), event_def
                        f = event_def['filter']
                        f = [f] if isinstance(f, str) else f
                        if not e.matches(*f):
                            continue
                    try:
                        self._call(event_def.get('call', event_name), e)
                    except Exception as ex:
                        log.error(ex)

    def _start(self, argv=None):
        """Startup of service. Default behavior: run named callable specified with 'start' in the service info.
        Otherwise, do nothing. It is alled after connecting to the message bus and initializing the 
        configuration, but before starting schedules and listening to commands and events."""
        if 'start' in self._sd:
            self._call(self._sd.start, argv)

    def start(self, argv=None):
        """Start service."""

        # Check uid (if applicable.)
        if 'checkuid' in self._sd:
            assert isinstance(self._sd.checkuid, int), self._sd.checkuid
        if not _os.geteuid() == self._sd.checkuid:
            _sys.exit(f'{self._sd.name} service must run with uid {self._sd.checkuid}')    

        log.info(f'{self._sd.name} service starting...')
        _signal.signal(_signal.SIGTERM, self.stop)
        _signal.signal(_signal.SIGINT, self.stop)

        # Connect to message bus.
        self._connect()

        # Init configuration.
        self.conf = ServiceConf(self._sd.get('conf'), self)

        self._start(argv)

        # Trigger schedules.
        if 'schedules' in self._sd:
            for sch_name in self._sd.schedules.keys():
                self._schedule(sch_name, init=True)

        # Start command listener.
        self._command_listener = _threading.Thread(target=self._listen_for_commands, daemon=True)
        self._command_listener.start()

        # Start event listener.
        if 'events' in self._sd and 'consume' in self._sd.events:
            self._event_listener = _threading.Thread(target=self._listen_for_events, daemon=True)
            self._event_listener.start()

        # Enter running mode.
        log.info(f'{self._sd.name} service running...')
        self._run()

    def _run(self):
        """Running service. Default behavior: run named callable specified with 'run' in the service info.
        Otherwise, wait on the command listener thread to terminate (i.e. do nothing.)"""
        if 'run' in self._sd:
            self._call(self._sd.run)
        else:
            self._command_listener.join()

    def _stop(self):
        """Stopping of service. Default behavior: run named callable specified with 'stop' in the service info.
        Otherwise, do nothing."""
        if 'stop' in self._sd:
            self._call(self._sd.stop)

    def stop(self, *_):
        """Stop service."""
        log.info(f'{self._sd.name} service stopping...')

        # Cancel schedules.
        for st in self._def_sched_timer.values():
            st.cancel()

        rc = self._stop()

        log.info(f'{self._sd.name} service stopped.')
        _sys.exit(rc)
