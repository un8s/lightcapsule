# Copyright 2020 info@un8s.com
# License: MIT, see LICENSE for details.

import datetime as _datetime
import itertools as _itertools
import platform as _platform
import re as _re
import time as _time
import uuid as _uuid

import psutil as _psutil

from .log import *

def timestamp():
    """Return a timestamp (with micro seconds precision)."""
    dt = _datetime.datetime.now()
    return int(dt.strftime('%Y%m%d%H%M%S%f'))

def uuid():
    """Return a uuid."""
    return str(_uuid.uuid4())

def filter_list(l, *fitems):
    """Return list of items of l filtered by given fitems (which can be regex patterns)."""
    return list(l) if len(fitems) == 0 else list(_itertools.chain(*[[i for i in l if _re.fullmatch(fex, i)] for fex in fitems]))

def match_list(l, *fitems):
    """Return True if any item of list l matches the given fitems (which can be regex patterns)."""
    return True if len(fitems) == 0 else len(filter_list(l, *fitems)) > 0


class AttrDict:
    """Kind of read-only dict with item and attribute access."""

    def __init__(self, d=None, **kwargs):
        assert isinstance(d, dict), d
        self._d = d or {}
        self._d.update(kwargs)

    def keys(self):
        return self._d.keys()

    def values(self):
        return self._d.values()

    def items(self):
        return self._d.items()

    def get(self, key, default=None):
        return self._d.get(key, default)

    def __getitem__(self, key):
        return self._d[key]

    def __getattr__(self, key):
        if key.startswith('__'):
            return super().__getattr__(key)
        return self._d[key]

    def __contains__(self, key):
        return key in self._d

    def __str__(self):
        return str(self._d)

    def __repr__(self):
        return self.__str__()


class CircuitBreaker:
    """Simple implementation of the circuit breaker pattern.
    
    >>> cb = CircuitBreaker()
    >>> while cb.is_closed():
    >>>     try:
    >>>         ...
    >>>         cb.reset()
    >>>         return Success
    >>>     except Error:
    >>>         cb.test()    
    >>>         return Failure
    """

    def __init__(self, name=None, fault_threshold=5, fault_timeout=5*60):
        self._name = name or self.__class__.__name__
        self._fault_threshold = fault_threshold
        self._fault_timeout = fault_timeout
        self._fault_time = None
        self._error_count = 0
        self._retry_count = 0

    @property
    def error_count(self):
        """Count of errors"""
        return self._error_count

    @property
    def is_faulty(self):
        """True if error count is not zero (breaker could be open or closed)"""
        return self._error_count == 0

    @property
    def retry_count(self):
        """Count of retries since opened"""
        return self._retry_count

    @property
    def recovery_time(self):
        """Time in seconds until next attempt to recover (if open)"""
        return 0.0 if not self._is_open else max(0.0, round(self._fault_timeout * 1.0 - ((timestamp()-self._fault_time) / 1000.0 / 1000.0), 2))

    @property
    def _is_open(self):
        """True if breaker is open"""
        return self._fault_time is not None

    def reset(self):
        """Manually reset breaker to closed state."""
        if self._is_open:
            log.debug(f'{self._name} reset')
        self._fault_time = None
        self._error_count = 0
        self._retry_count = 0

    def is_closed(self):
        """Return True if breaker is closed (or half-open)."""
        if self._is_open:
            if timestamp()-self._fault_time > self._fault_timeout*1000*1000:
                # "Half-open" breaker -> retry once.
                self._error_count -= 1
                self._retry_count += 1
                log.debug(f'{self._name} retry #{self._retry_count}')
        return self._error_count < self._fault_threshold

    def test(self):
        """Increase error count and test if breaker should be opened."""
        if not self._is_open:
            self._error_count += 1
            log.debug(f'{self._name} test #{self._error_count}')
            if self._error_count == self._fault_threshold:
                self._fault_time = timestamp()
                log.debug(f'{self._name} open')


from .mbus import *

def sys_info(prefix='sys'):
    """Return dict with system info."""
    si = {}
    si[f'{prefix}.kernel'] = _platform.release()
    with open('/etc/machine-id', 'r') as f:
        si[f'{prefix}.serial'] = f.read()
    dt = _datetime.datetime.fromtimestamp(_psutil.boot_time())
    si[f'{prefix}.boot_time'] = int(dt.strftime('%Y%m%d%H%M%S%f'))
    return si
