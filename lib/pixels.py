# Copyright 2020 info@un8s.com
# License: MIT, see LICENSE for details.

import threading as _threading

_pwm_supported, _spi_supported = False, False
try:
    import board as _board
    import neopixel as _neopixel
    _pwm_supported = True
except ImportError:
    pass

try:
    import spidev as _spidev
    _spi_supported = True
except ImportError:
    pass

import webcolors as _webcolors

# IMPORTANT: There should be only one process in the system that writes to the pixel array!


__all__ = ['Palette', 'Pixels']


# rgb = (0-255, 0-255, 0-255)
# rgbf = (0.0-1.0, 0.0-1.0, 0.0-1.0)

def _f(x, ceil=100):
    """Return float 0.0..1.0 from float or int."""
    assert isinstance(x, float) and 0.0 <= x <= 1.0 or isinstance(x, int) and 0 <= x <= ceil, x
    return x if isinstance(x, float) else round(x/ceil, 2)

def _rgbf(rgbx, brightness=None):
    """Return rgbf from rgbf or rgb, optionally adjusted by brightness (0.0-1.0 or 0-100)."""
    assert isinstance(rgbx, tuple) and len(rgbx) == 3, rgbx
    bf = 1.0 if brightness is None else _f(brightness)
    rgbf = []
    for i in range(len(rgbx)):
        rgbf.append(round(_f(rgbx[i], 255) * bf, 2))
    return tuple(rgbf)

def _rgb(rgbx):
    """Return rgb from float or int."""
    assert isinstance(rgbx, tuple) and len(rgbx) == 3, rgbx
    rgb = []
    for i in range(len(rgbx)):
        assert isinstance(rgbx[i], int) and 0 <= rgbx[i] <= 255 or isinstance(rgbx[i], float) and 0.0 <= rgbx[i] <= 1.0, rgbx
        rgb.append(rgbx[i] if isinstance(rgbx[i], int) else int(rgbx[i]*255))
    return tuple(rgb)


class Palette:
    """Helper class to convert color names to RGB and vice versa."""

    @staticmethod
    def rgbf(name):
        """Return rgbf for a named color. Raise ValueError if color name is unknown."""
        return _rgbf(_webcolors.name_to_rgb(name))

    @staticmethod
    def _closest_name(rgb):
        """Return name of known color matching closest the given RGB."""
        min_colours = {}
        for key, name in _webcolors.CSS3_HEX_TO_NAMES.items():
            r_c, g_c, b_c = _webcolors.hex_to_rgb(key)
            rd = (r_c - rgb[0]) ** 2
            gd = (g_c - rgb[1]) ** 2
            bd = (b_c - rgb[2]) ** 2
            min_colours[(rd + gd + bd)] = name
        return min_colours[min(min_colours.keys())]

    @staticmethod
    def name(rgbx, closest=True):
        """Return color name for RGB. Raturn closest color or raise ValueError."""
        rgb = _rgb(rgbx)
        try:
            name = _webcolors.rgb_to_name(rgb)
        except ValueError:
            if not closest: raise
            name = Palette._closest_name(rgb)
        return name

    @staticmethod
    def check_name(name):
        """Return color name if known. Raise ValueError otherwise."""
        webcolors.name_to_rgb(name)    # Will throw ValueError for unknown color names.
        return name


class _PwmPixels:
    """Pixels connected to a PWM-enabled GPIO."""

    def __init__(self, pin, size, auto_update=True):
        assert isinstance(size, int) and size > 0, size
        self._size = size
        self._current = [(0,0,0)]*self._size    # Assuming all are off.
        self._neo_pixels = _neopixel.NeoPixel(getattr(_board, pin) if isinstance(pin, str) else pin, size, auto_write=not auto_update)

    def __del__(self):
        # Release hardware.
        self._neo_pixels.fill((0, 0, 0))
        self._neo_pixels.deinit()

    def __len__(self):
        return self._size

    def __getitem__(self, index):
        """Return current pixel RGB (may not be updated yet.)"""
        assert isinstance(index, int) and 0 <= index < self._size, index
        return self._current[index]

    def __setitem__(self, index, rgb):
        """Set current pixel RGB."""
        assert isinstance(index, int) and 0 <= index < self._size, index
        self._current[index] = rgb
        self._neo_pixels[index] = rgb

    def update(self):
        """Update actual pixel state."""
        self._neo_pixels.show()


class _SpiPixels:
    """Pixels connected to SPI MOSI."""

    def __init__(self, bus_device, size, auto_update=True):
        assert isinstance(size, int) and size > 0, size
        self._auto_update = auto_update
        self._size = size
        self._current = [(0,0,0)]*self._size    # Assuming all are off.
        self._spi = _spidev.SpiDev()
        self._spi.open(*bus_device)

    def __del__(self):
        # Release hardware.
        self._current = [(0,0,0)]*self._size
        self.update()
        self._spi.close()

    def __len__(self):
        return self._size

    def __getitem__(self, index):
        """Return current pixel RGB (may not be updated yet.)"""
        assert isinstance(index, int) and 0 <= index < self._size, index
        return self._current[index]

    def __setitem__(self, index, rgb):
        """Set current pixel RGB."""
        assert isinstance(index, int) and 0 <= index < self._size, index
        self._current[index] = rgb
        if self._auto_update:
            self.update()

    def update(self):
        """Update actual pixel state."""
        tx = []
        for rgb in self._current:
            for byte in (rgb[1], rgb[0], rgb[2]):
                for ibit in range(7, -1, -1):
                    tx.append(((byte>>ibit)&1)*0x78 + 0x80)
        self._spi.xfer(tx, int(8/1.25e-6))


class Pixels:
    """Control WS2812 LED pixels.
    
    >>> p = Pixels('PIN18', (1,8))  # One row of 8 LEDs on GPIO pin 18.
    >>> p = Pixels((0,0), (8,8), 2) # 8x8 LED matrix on SPI bus 0, device 0.

    If multiple overlays are used, the display always shows the combination of all overlays with
    higher overlay numbers having higher priority (e.g. the color in overlay 2 will overwrite the
    color in overly for the same pixel). Overlay numbering starts with 1. 
    """

    has_pwm = _pwm_supported
    has_spi = _spi_supported

    def __init__(self, hw, size, overlays=1, auto_update=True):
        assert isinstance(size, tuple) and len(size) == 2 and isinstance(size[0], int) and isinstance(size[1], int), size
        assert isinstance(overlays, int) and 1 <= overlays, overlays
        self._sx, self._sy = size
        self._sz = overlays
        self._active = [None]*self.size
        self._overlay = [[None]*self.size for i in range(0, self._sz)]
        self._brightness = 1.0

        self._hw_lock = _threading.RLock()
        self._hw_pixels = _SpiPixels(hw, self._sx*self._sy, auto_update) if isinstance(hw, tuple) else _PwmPixels(hw, self._sx*self._sy, auto_update)
    
    @property
    def size(self):
        return self._sx*self._sy

    @property
    def sx(self):
        return self._sx

    @property
    def sy(self):
        return self._sy

    @property
    def brightness(self):
        return self._brightness

    @brightness.setter
    def brightness(self, bx):
        self._brightness = _f(bx)

    def update(self, bx=None):
        """Update LEDs. Optionally change general brightness."""
        if bx is not None:
            self._brightness = _f(bx)

        def _or(a, l):
            return a or _or(l.pop(), l) if len(l) > 0 else a

        # Determine active display by combining overlays.
        old_active = list(self._active)
        for p in range(0, self.size):
            self._active[p] = _or(None, [self._overlay[ov][p] for ov in range(0, self._sz)])

        # Update pixels if active display has changed.
        if bx is not None or self._active != old_active:
            with self._hw_lock:
                for p in range(0, self.size):
                    if bx is not None or self._active[p] != old_active[p]:
                        r, g, b = self._active[p] or (0.0, 0.0, 0.0)
                        self._hw_pixels[p] = (int(r*self._brightness*255), int(g*self._brightness*255), int(b*self._brightness*255))
                self._hw_pixels.update()

    def _rgbf(self, color, brightness):
        """Return rgbf from color (i.e. rgbx or name)."""
        return _rgbf(Palette.rgbf(color) if isinstance(color, str) else color, brightness)

    def fill(self, ov=None, color=None, brightness=None, update=True):
        """Fill one or all overlays. If color is None, pixels are cleared. brightness can be used to
        overwrite the display's general brightness."""
        assert ov is None or isinstance(ov, int) and 1 <= ov <= self._sz, ov
        rgbf = None if color is None else self._rgbf(color, brightness)

        for i in range(0, self._sz) if ov is None else [ov-1]:
            self._overlay[i] = [rgbf]*self.size

        if update:
            self.update()

    def clear(self, ov=None, update=True):
        """Clear one or all overlays."""
        self.fill(ov, update=update)

    def set(self, ov, pos, color=None, brightness=None, update=True):
        """Set pixel(s) in overlay. pos can be a linear position (1..N), a tuple of coordinates (1..SX,1..SY),
        or a list of these. If color is None, pixels are cleared. brightness can be used to overwrite the
        display's general brightness."""
        assert isinstance(ov, int) and 1 <= ov <= self._sz, ov
        assert isinstance(pos, (int, list, tuple)), pos
        rgbf = None if color is None else self._rgbf(color, brightness)

        if isinstance(pos, int):
            assert 1 <= pos <= self.size, pos
            self._overlay[ov-1][pos-1] = rgbf
        elif isinstance(pos, tuple):
            assert len(pos) == 2 and 1 <= pos[0] <= self.sx and 1 <= pos[1] <= self.sy, pos
            self._overlay[ov-1][(pos[0]-1)*self.sy+(pos[1]-1)] = rgbf
        else:
            for p in pos:
                self.set(ov, p, color, brightness, update=False)

        if update:
            self.update()
