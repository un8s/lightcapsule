# Copyright 2020 info@un8s.com
# License: MIT, see LICENSE for details.

import os as _os
import pickle as _pickle
import re as _re
import socket as _socket
import sys as _sys
import threading as _threading
import time as _time

import redis as _redis

from lib import log, filter_list, match_list, timestamp, uuid, AttrDict

__all__ = ['UndeliveredError', 'BrokerError', 'Event', 'Request', 'Response', 'MessageBus']

_MBUS_VERSION = '20.12'

class Event(AttrDict):
    """Tagged event."""

    def __init__(self, topic, timestamp, host, tags, data):
        assert isinstance(topic, str) and len(topic), topic
        assert isinstance(timestamp, int) and timestamp > 0, timestamp
        assert isinstance(tags, list), tags
        assert isinstance(data, dict), data
        super().__init__(data)
        self._topic = topic
        self._timestamp = timestamp
        self._host = host
        self._tags = tags

    @property
    def topic(self):
        """Return topic for which event was sent/received."""
        return self._topic

    @property
    def host(self):
        """Return host from which event was sent."""
        return self._host

    @property
    def timestamp(self):
        """Return timestamp at which event was sent."""
        return self._timestamp

    def __lt__(self, other):
        return self.timestamp < other.timestamp

    def iso_timestamp(self, timespec='minutes'):
        """Return ISO formatted timestamp with timespec (minutes|seconds|micro) precision."""
        t = str(self._timestamp)
        iso = f'{t[0:4]}-{t[4:6]}-{t[6:8]} {t[8:10]}:{t[10:12]}'
        if timespec == 'seconds':
            iso += f':{t[12:14]}'
        if timespec == 'micro':
            iso += f'.{t[14:]}'
        return iso

    def tags(self, *tags):
        """Return [tags] of event, optionally filtered by specified tags (which can be regex patterns)."""
        return filter_list(self._tags, *tags)

    def matches(self, *tags):
        """Return True if any event tag matches the specified tags (which can be regex patterns)."""
        return match_list(self._tags, *tags)

    def data(self):
        """Return {data} of event."""
        return dict(self._d)

    def error(self):
        """Return 'error' of {data} if present, else None."""
        return self._d.get('error')

    def __str__(self):
        return f'{self.__class__.__name__}({self._host}, {self._topic}, {self._timestamp}, {self._tags}, {self._d})'


class Request(Event):

    def __init__(self, topic, timestamp, host, tags, _response_topic, data):
        assert isinstance(_response_topic, str) and len(_response_topic), _response_topic
        self._response_topic = _response_topic
        Event.__init__(self, topic, timestamp, host, tags, data)


class Response(Event): ...


class UndeliveredError(Exception): ...
"""Request or response could not be delievered."""


class BrokerError(Exception): ...
"""Error at message bus broker."""


class _MessageBusStore:
    """KV store on the message bus."""

    def __init__(self, mbus):
        self._mbus = mbus

    def _value_topic(self, topic, key):
        """Return broker topic for value key."""
        return f'{self._mbus._app}:topic:{topic}:value:{key}'

    def keys(self, pattern=None, topic=None):
        """Return list of value keys for topic."""
        assert topic is None or isinstance(topic, str) and len(topic), topic

        t = topic or self._mbus._topic
        assert t is not None, 'Missing topic'

        keys = []
        for k in self._mbus._monitor('scan_iter', self._value_topic(t, '*')):
            k = k.decode('utf-8').split(':')[-1]
            if pattern is None or _re.fullmatch(pattern, k):
                keys.append(k)
        return keys

    def get(self, key, topic=None):
        """Return data for value key, or None."""
        assert isinstance(key, str) and len(key), key
        assert topic is None or isinstance(topic, str) and len(topic), topic

        t = topic or self._mbus._topic
        assert t is not None, 'Missing topic'

        rv = self._mbus._monitor('get', self._value_topic(t, key))
        return None if rv is None else _pickle.loads(rv)

    def set(self, key, value, topic=None):
        """Store value key for topic."""
        assert isinstance(key, str) and len(key), key
        assert topic is None or isinstance(topic, str) and len(topic), topic

        t = topic or self._mbus._topic
        assert t is not None, 'Missing topic'

        self._mbus._monitor('set', self._value_topic(t, key), _pickle.dumps(value))

    def delete(self, key, topic=None):
        """Delete value key from topic."""
        assert isinstance(key, str) and len(key), key
        assert topic is None or isinstance(topic, str) and len(topic), topic

        t = topic or self._mbus._topic
        assert t is not None, 'Missing topic'

        self._mbus._monitor('delete', self._value_topic(t, key))


class MessageBus:
    """Message bus between services with Redis as broker."""

    version = _MBUS_VERSION

    def __init__(self, app, topic=None, *, broker_host='localhost', broker_port=6379, broker=None):
        assert isinstance(app, str) and len(app), app
        assert topic is None or isinstance(topic, str) and len(topic), topic
        self._app = app
        self._topic = topic
        self._broker_host, self._broker_port = broker_host, broker_port
        self._broker_lock = _threading.RLock()
        self._sender_host = _socket.gethostname()
        self.reconnect(broker=broker)
        self._store = _MessageBusStore(self)

    @property
    def store(self):
        return self._store

    def reconnect(self, broker_host=None, broker_port=None, broker=None):
        """Reconnect broker."""
        self._broker_host = broker_host or self._broker_host
        self._broker_port = broker_port or self._broker_port
        with self._broker_lock:
            self._broker = broker or _redis.Redis(host=self._broker_host, port=self._broker_port, health_check_interval=60)
            self._event_subscriptions = {}
            self._request_subscriptions = {}

    def _topic_name(self, broker_topic):
        """Return topic name from broker topic."""
        if not isinstance(broker_topic, str):
            broker_topic = str(broker_topic, encoding='utf-8')
        nt = broker_topic.split(':')
        assert len(nt) > 3 and nt[0] == self._app and nt[1] == 'topic', nt
        return nt[2]

    def _event_topic(self, topic):
        """Return broker topic for event."""
        return f'{self._app}:topic:{topic}:event'

    def _index_topic(self, topic):
        """Return broker topic for index."""
        return f'{self._app}:topic:{topic}:index'

    def _store_topic(self, topic):
        """Return broker topic for store."""
        return f'{self._app}:topic:{topic}:store'

    def _monitor(self, method, *args, **kwargs):
        try:
            with self._broker_lock:
                r = getattr(self._broker, method)(*args, **kwargs)
            margs = list(args)
            if len(margs) > 1 and isinstance(margs[1], bytes):
                margs[1] = _pickle.loads(margs[1])
            mdata = {
                'pid': _os.getpid(),
                'host': self._sender_host,
                'topic': self._topic,
                'method': method,
                'args': margs,
                'kwargs': kwargs
            }
            if method in ('get',):
                if r is None:
                    mdata['return'] = None
                else:
                    mdata['return'] = _pickle.loads(r)
            elif method in ('publish',):
                mdata['return'] = r
            try:
                with self._broker_lock:
                    self._broker.publish(f'{self._app}:_monitor', _pickle.dumps(mdata))
            except:
                pass
            return r
        except Exception as ex:
            raise BrokerError(ex)

    def _monitor_sub(self, sub, method, *args, **kwargs):
        try:
            r = getattr(sub, method)(*args, **kwargs)
            mdata = {
                'pid': _os.getpid(),
                'host': self._sender_host,
                'topic': self._topic,
                'method': method,
                'args': args,
                'kwargs': kwargs,
                'topics': [str(ch, encoding='utf-8') for ch in sub.channels.keys()]
            }
            if method in ('get_message',):
                if r is None:
                    mdata['return'] = None
                elif r['type'] == 'message':
                    mdata['return'] = _pickle.loads(r['data'])
                else:
                    mdata['return'] = r
            try:
                with self._broker_lock:
                    self._broker.publish(f'{self._app}:_monitor', _pickle.dumps(mdata))
            except:
                pass
            return r
        except Exception as ex:
            raise BrokerError(ex)

    def monitor(self, *, topics=None, methods=None, hosts=None, format=None, ignore_return_none=False):
        """Print monitored mbus messages until interrupted."""
        with self._broker_lock:
            ms = self._broker.pubsub(ignore_subscribe_messages=True)
        ms.subscribe(f'{self._app}:_monitor')
        while True:
            m = ms.get_message(timeout=1.0)
            if m:
                mdata = _pickle.loads(m['data'])
                if topics is not None and mdata['topic'] not in topics:
                    continue
                if methods is not None and mdata['method'] not in methods:
                    continue
                if hosts is not None and mdata['host'] not in hosts:
                    continue
                if 'return' in mdata and mdata['return'] is None and ignore_return_none:
                    continue
                odata = {k:mdata.get(k, '-') for k in ('pid', 'host', 'topic', 'method', 'args', 'kwargs', 'topics', 'return')}
                of = format or '{pid} {host} {topic} {method} {args} {kwargs} {topics} {return}'
                print(of.format(**odata))

    def _push_event(self, data, topics, tags, notify, persist):
        assert isinstance(data, dict), data
        assert topics is None or isinstance(topics, (str, list)) and len(topics), topics
        assert isinstance(tags, list), tags

        if topics is None:
            assert self._topic is not None, 'Missing topic'
            topics = [self._topic]
        elif isinstance(topics, str):
            topics = [topics]
        if self._topic is not None and self._topic not in topics:
            topics.insert(0, self._topic)

        d = {'_timestamp': timestamp(), '_host': self._sender_host, '_tags': tags}
        d.update(data)

        for t in topics:
            if persist:
                self._monitor('rpush', self._index_topic(t), d['_timestamp'])
                self._monitor('rpush', self._store_topic(t), _pickle.dumps(d))

            if notify:
                ns = self._monitor('publish', self._event_topic(t), _pickle.dumps(d))
                log.debug(f'Sent event for topic \'{t}\' (delivered to {ns}): {d}')

        return d['_timestamp']

    def send_event(self, data, topics=None, *, tags=None, persist=False):
        """Return timestamp of event after sending it to all specified topics, including own topic."""
        return self._push_event(data, topics, tags or [], True, persist)

    def store_event(self, data, topics=None, *, tags=None, notify=False):
        """Return timestamp of event after storing it to all specified topics, including own topic."""
        return self._push_event(data, topics, tags or [], notify, True)

    def _receive(self, sub, tags, timeout):
        """Return (topic, data) or None from subscription. Wait at max timeout seconds before returning.
        Optionally filter for tags. Raise BrokerError."""
        assert tags is None or isinstance(tags, list), tags
        tags = tags or []
        while True:
            m = self._monitor_sub(sub, 'get_message', timeout=timeout)
            if m:
                if m['type'] != 'message':
                    continue
                mdata = _pickle.loads(m['data'])
                assert isinstance(mdata, dict), mdata
                mt = self._topic_name(m['channel'])
                if match_list(mdata.get('_tags', []), *tags):
                    log.debug(f'Got: {mt},{mdata}')
                    return mt, mdata
                log.debug(f'Ignore: {mt},{mdata}')
            return None

    def get_event(self, topics, *, tags=None, timeout=0.0):
        """Return event if available on specified topic(s) or None. Wait at max timeout seconds before returning.
        Optionally filter for tags."""
        assert isinstance(topics, (str, list)) and len(topics), 'Missing topics'

        if isinstance(topics, str):
            topics = [topics]

        try:
            # Implicitly create new subscription to (group of) event topic(s) if necessary.
            ps_key = ','.join(sorted(topics))
            with self._broker_lock:
                ps = self._event_subscriptions.get(ps_key, None)
            if ps is None:
                ps = self._monitor('pubsub', ignore_subscribe_messages=False)
                self._monitor_sub(ps, 'subscribe', *[self._event_topic(t) for t in topics])
            with self._broker_lock:
                self._event_subscriptions[ps_key] = ps

            ps.check_health()
            e = self._receive(ps, tags, timeout)
            return None if e is None else Event(e[0], e[1].pop('_timestamp'), e[1].pop('_host'), e[1].pop('_tags'), e[1])
        except BrokerError as ex:
            # Remove erroneous subscription.
            with self._broker_lock:
                self._event_subscriptions[ps_key] = None
            raise
        
    def stored_topics(self, *topics):
        """Return list of stored topics, optionally filtered by specified topic patterns."""
        result = []
        for bt in self._monitor('scan_iter', self._store_topic('*')):
            t = self._topic_name(bt)
            if len(topics) == 0 or any(filter(lambda p: _re.fullmatch(p, t), topics)):
                result.append(t)
        return result

    # TODO Read "since" timestamp.
    def read_events(self, topics=None, *, tags=None, n=None, reverse=None):
        """Return list of stored events from topics, optionally filtered by tags. Read own events if
        no topic specified. Read events of all known topics if topic '*' is specified.
        The result list contains at most abs(n) elements, beginning with the oldest if n > 0 or
        ending with the newest if n < 0.
        """
        assert topics is None or isinstance(topics, (str, list)), topics
        assert tags is None or isinstance(tags, list), tags
        assert n is None or isinstance(n, int) and n != 0, n

        if topics is None:
            topics = [self._topic]
        elif topics == '*':
            topics = self.stored_topics()
        elif isinstance(topics, str):
            topics = [topics]
        assert len(topics), 'Missing topic'
        tags = tags or []

        events = []
        for t in topics:
            t_len = self._monitor('llen', self._store_topic(t))
            if n is None:
                start, end = 0, -1
            elif n > 0:
                start, end = 0, min(n-1, t_len-2)
            elif n < 0:
                start, end = max(0, t_len+n), t_len-1
                if reverse is None:
                    reverse = True

            for m in self._monitor('lrange', self._store_topic(t), start, end):
                mdata = _pickle.loads(m)
                e = Event(t, mdata.pop('_timestamp'), mdata.pop('_host'), mdata.pop('_tags'), mdata)
                if e.matches(*tags):
                    events.append(e)

        events.sort(reverse=reverse or False)
        return events

    def purge_events(self, topic=None):
        """Purge stored events from topic."""
        t = topic or self._topic
        assert t is not None, 'Missing topic'
        self._monitor('delete', self._index_topic(t), self._store_topic(t))

    def _request_topic(self, topic):
        """Return broker topic for request."""
        return f'{self._app}:topic:{topic}:request'

    def _response_topic(self, topic):
        """Return new unique broker topic for response."""
        return f'{self._app}:topic:{topic}:response:{uuid()}'

    def send_request(self, data, topic, *, tags=None, must_deliver=True, combine=True, timeout=0.0):
        """Return response(s) after sending request for topic. Wait at max timeout seconds before returning.
        If combine is True, return one combined response, or None. If combine is False, return a list of responses,
        or empty list. Raise UndeliveredError if request was not delivered and must_deliver is True.
        Return None or emtpy list if must_deliver is False. Raise BrokerError."""
        assert isinstance(data, dict), data
        assert tags is None or isinstance(tags, list), tags
        assert isinstance(topic, str) and len(topic), topic

        d = {'_timestamp': timestamp(), '_host': self._sender_host, '_tags': tags or [], '_response_topic': self._response_topic(topic)}
        d.update(data)

        # Ad-hoc subscription to receive response.
        ps = self._monitor('pubsub', ignore_subscribe_messages=False)
        self._monitor_sub(ps, 'subscribe', d['_response_topic'])

        try:
            # Send request.
            ns = self._monitor('publish', self._request_topic(topic), _pickle.dumps(d))
            log.debug(f'Sent request for topic \'{topic}\' (delivered to {ns}): {d}')
            if ns == 0:
                if must_deliver:
                    raise UndeliveredError()
                return None if combine else []

            # Receive response(s).
            rlist = []
            while len(rlist) < ns:
                r = self._receive(ps, [], timeout)
                log.debug(f'  response {len(rlist)+1}/{ns}: {r}')
                if r is None:
                    break
                rlist.append(r)        
        finally:
            ps.close()

        if combine:
            if len(rlist) == 0:
                return None
            rtags, rdata = [], {}
            for r in rlist:
                ts = r[1].pop('_timestamp')
                r[1].pop('_host')
                rtags += r[1].pop('_tags')
                rdata.update(r[1])
            return Response(rlist[0][0], ts, '*', list(set(rtags)), rdata)

        return [Response(r[0], r[1].pop('_timestamp'), r[1].pop('_host'), r[1].pop('_tags'), r[1]) for r in rlist]

    def get_request(self, topic=None, *, tags=None, timeout=0.0):
        """Return request if available for given or own topic or None. Wait at max timeout seconds before returning.
        Optionally filter for tags."""
        assert topic is None or isinstance(topic, str) and len(topic), topic

        t = topic or self._topic
        assert t is not None, 'Missing topic'

        try:
            # Implicitly create new subscription to request topic if necessary.
            with self._broker_lock:
                ps = self._request_subscriptions.get(t, None)
            if ps is None:
                ps = self._monitor('pubsub', ignore_subscribe_messages=False)
                self._monitor_sub(ps, 'subscribe', self._request_topic(t))
                with self._broker_lock:
                    self._request_subscriptions[t] = ps

            ps.check_health()
            r = self._receive(ps, tags, timeout)
            return None if r is None else Request(r[0], r[1].pop('_timestamp'), r[1].pop('_host'), r[1].pop('_tags'), r[1].pop('_response_topic'), r[1])
        except BrokerError as ex:
            # Remove erroneous subscription.
            with self._broker_lock:
                self._request_subscriptions[t] = None
            raise

    def send_response(self, data, request, *, must_deliver=False):
        """Send response for request.
        Raise UndeliveredError if response was not delivered and must_deliver is True."""
        assert isinstance(data, dict), data
        assert isinstance(request, Request), request

        d = {'_timestamp': timestamp(), '_host': self._sender_host, '_tags': request.tags()}
        d.update(data)

        ns = self._monitor('publish', request._response_topic, _pickle.dumps(d))
        log.debug(f'Sent response for topic \'{request.topic}\' (delivered to {ns}): {d}')
        if must_deliver and ns == 0:
            raise UndeliveredError()


if __name__ == '__main__':
    """Useful for testing:

    Send request to the message bus and wait for response:
    > mbus send_request <app> <topic> (key=value)* [--no-combine] [--timeout=<timeout in s>]

    Connect to message bus and print all activity for specific app to stdout:
    > mbus monitor <app> [--topics=<topic>,..] [--methods=<method>,..] [--hosts==<host>,..] [--ignore-return-none] [--format=<output format>]

        Optionally filter for topics, sender hosts, and methods (subscribe, get, set, delete, rpush, publish, pubsub, lrange, llen, scan_iter)
        Default output format: '{pid} {host} {topic} {method} {args} {kwargs} {topics} {return}'
        --ignore-return-none: Don't print if method call returned None
    """

    if len(_sys.argv) < 2 or _sys.argv[1] not in ('send_request', 'monitor'):
        _sys.exit(f'usage: {_sys.argv[0]} send_request|monitor')

    if _sys.argv[1] == 'send_request':
        pos, app, topic = 0, None, None
        data = {}
        combine = True
        timeout = 10
        if len(_sys.argv) > 2:
            for arg in _sys.argv[2:]:
                if arg.startswith('--timeout='):
                    timeout = int(arg[len('--timeout='):])
                elif arg == '--no-combine':
                    combine = False
                elif '=' in arg:
                    key, value = arg.split('=')
                    if value in ('True', 'False'):
                        data[key] = value == 'True'
                    elif _re.fullmatch('-?\d+', value):
                        data[key] = int(value)
                    elif _re.fullmatch('-?\d+.\d+', value):
                        data[key] = float(value)
                    else:
                        data[key] = value
                else:
                    if pos == 0:
                        app = arg
                        pos += 1
                    elif pos == 1:
                        topic = arg
                        pos += 1
                    else:
                        app = None
                        break
        if app is None or topic is None or len(data) == 0:
            _sys.exit(f'usage: {_sys.argv[0]} send_request <app> <topic> (key=value)* [--no-combine] [--timeout=<timeout in s>]')
        try:
            mbus = MessageBus(app)
            print(mbus.send_request(data, topic, combine=combine, timeout=timeout, must_deliver=True))
        except BrokerError:
            pass
        _sys.exit(0)

    if _sys.argv[1] == 'monitor':
        pos, app = 0, None
        topics, methods, hosts, format = None, None, None, None
        ignore_return_none = False
        if len(_sys.argv) > 2:
            for arg in _sys.argv[2:]:
                if arg.startswith('--topics='):
                    topics = arg[len('--topics='):].split(',')
                elif arg.startswith('--methods='):
                    methods = arg[len('--methods='):].split(',')
                elif arg.startswith('--hosts='):
                    hosts = arg[len('--hosts='):].split(',')
                elif arg.startswith('--format='):
                    format = arg[len('--format='):]
                elif arg == '--ignore-return-none':
                    ignore_return_none = True
                else:
                    if pos == 0:
                        app = arg
                        pos += 1
                    else:
                        app = None
                        break
        if app is None:
            _sys.exit(f'usage: {_sys.argv[0]} monitor <app> [--topics=<topic>,..] [--methods=<method>,..] [--hosts==<host>,..] [--ignore-return-none] [--format=<output format>]')
        mbus = MessageBus(app)
        try:
            mbus.monitor(topics=topics, methods=methods, hosts=hosts, ignore_return_none=ignore_return_none, format=format)
        except KeyboardInterrupt:
            pass
        _sys.exit(0)
