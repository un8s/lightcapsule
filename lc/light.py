# Copyright 2020 info@un8s.com
# License: MIT, see LICENSE for details.

import enum
import os
import threading
import random
import sys
import time

from lib import log
from lib.pixels import Palette, Pixels
from lib.srv import ServiceDef, Service, ServiceError

from disk import SDEF as SDISK
from health import SDEF as SHEALTH
from backup import SDEF as SBACKUP

# Low-level configuration from environment.
_PIN = os.getenv('LC_LIGHT_PIN', (0,0))


_LOGO = [
    (1, 1), (1, 2), (1, 3), (1, 4), (1, 5), (1, 6),
    (2, 1), (2, 7),
    (3, 1), (3, 5), (3, 8),
    (4, 1), (4, 5), (4, 8),
    (5, 1), (5, 5), (5, 8),
    (6, 1), (6, 5), (6, 8),
    (7, 1), (7, 7),
    (8, 1), (8, 2), (8, 3), (8, 4), (8, 5), (8, 6)
]

_POWER = [
    (1, 3), (1, 4), (1, 5), (1, 6),
    (2, 2), (2, 7),
    (3, 1),
    (4, 1), (4, 5), (4, 6), (4, 7), (4, 8),
    (5, 1),# (5, 5), (5, 6), (5, 7), (5, 8),
    (6, 1),
    (7, 2), (7, 7),
    (8, 3), (8, 4), (8, 5), (8, 6)
]

def _check_brightness(value):
    if not (0 <= value <= 100):
        raise ValueError(f"Value for 'light.brightness' must be between 0 and 100: {value}")
    return value


SDEF = ServiceDef({
    'app': 'lc',
    'name': 'light',
    'version': '20.12',
    'description': 'Light and status display service.',
    'checkuid': 0,
    'mbus': { 'broker.name': '_lc' },
    'conf': {
        'light.color': {
            'value': 'khaki',
            'type': str,
            'check': Palette.check_name,
            'required': True
        },
        'light.brightness': {
            'value': 10,
            'type': int,
            'check': _check_brightness,
            'required': True
        },
        'hd.color': {
            'value': 'green',
            'type': str,
            'check': Palette.check_name,
            'required': True
        },
        'bk.color': {
            'value': 'blue',
            'type': str,
            'check': Palette.check_name,
            'required': True
        }
    },
    'commands': {
        'light': {
            'args': {
                'switch': { 'type': str, 'choices': ['on', 'off', 'toggle'] },
                'brightness': { 'type': int, 'range': (0, 100) },
                'color': { 'type': str, 'check': Palette.check_name },
                'red': { 'type': float, 'range': (0.0, 1.0) },
                'green': { 'type': float, 'range': (0.0, 1.0) },
                'blue': { 'type': float, 'range': (0.0, 1.0) }
            }
        },
        'state': {}
    },
    'events': {
        'consume': {
            'on_disk_access': {
                'source': SDISK,
                'filter': '.*\.access'
            },
            'on_shares_access': {
                'source': SDISK,
                'filter': 'hd\.shares'
            },
            'on_disk_health': {
                'source': SDISK,
                'filter': '.*\.health'
            },
            'on_alert': {
                'source': [SDISK, SHEALTH, SBACKUP],
                'filter': 'alert\..*'
            },
            'on_system_shutdown': {
                'source': SHEALTH,
                'filter': 'system\.shutdown'
            }
        },
        'emit': {
            'state': {
                'args': {
                    'on': { 'type': bool },
                    'brightness': { 'type': int }
                }
            }
        }
    }
})


class _Overlay(enum.IntEnum):
    light = 1
    status = 2
    alert = 3

class _LightService(Service):
    """Light and status display service."""

    def __init__(self):
        super().__init__(SDEF)
        self._display = Pixels(_PIN, (8,8), overlays=3)
        self._disk_health = {}
        self._alerts = {}

    def on_disk_access(self, e):
        """Trigger display when disk is accessed."""
        assert e.matches('.*\.access'), e
        if e.error() is None:
            if e.write_access is None:
                self._display.set(_Overlay.status, [(1, y) for y in range(1, self._display.sy)], None)
                self._display.set(_Overlay.status, [(self._display.sx, y) for y in range(1, self._display.sy)], None)
            else:
                load = f'{e.name}.load'
                if load in self._disk_health:
                    x = 1 if e.name == 'hd' else self._display.sx
                    p = [(x, y) for y in range(1, max(1, int((self._disk_health[load]+1)/100.0*self._display.sy))+1)]
                    self._display.set(_Overlay.status, p, Palette.rgbf(self.conf[f'{e.name}.color']))

    def on_shares_access(self, e):
        """Update shares info."""
        assert e.matches('hd.shares'), e
        if e.error() is None:
            self._display.set(_Overlay.status, (3, 1), Palette.rgbf('LimeGreen') if 'share' in e.shares else None)
            self._display.set(_Overlay.status, (5, 1), Palette.rgbf('DeepSkyBlue') if 'timemachine' in e.shares else None)

    def on_disk_health(self, e):
        """Save current disk health for load information."""
        assert e.matches('.*\.health'), e
        if e.error() is None:
            self._disk_health.update(e.data())

    def on_alert(self, e):
        """Handle alerts."""
        assert e.matches('alert\..*'), e
        alert = e.tags('alert\..*')
        if alert:
            akey = alert[0][len('alert.'):]
            if akey in e:
                aval = e[akey]
                if aval is not None:
                    self._alerts[akey] = aval
                else:
                    self._alerts.pop(akey, None)

            # Determine alert level.
            if len(self._alerts):
                self._display.set(_Overlay.alert, (4, 4), 'red')
            else:
                self._display.clear(_Overlay.alert)

    def on_system_shutdown(self, e):
        """Handle system shutdown."""
        assert e.matches('system\.shutdown'), e
        self._display.set(_Overlay.status, _POWER, 'Crimson')
        threading.Timer(2.0, self._display.clear).start()

    def do_state(self):
        return self.response(**self._state)

    def do_light(self, switch, brightness, color, red, green, blue):
        old_state = dict(self._state)

        if switch is not None:
            if switch == 'on':
                self._state['on'] = True
            elif switch == 'off':
                self._state['on'] = False
            else:
                self._state['on'] = not self._state['on']

        if brightness is not None:
            if brightness < 10:
                self._state['on'] = False
                brightness = 10
            self._state['brightness'] = brightness
            self._display.update(brightness)

        if color is not None:
            self._state['color'] = color

        if red or green or blue:
            self._state['color'] = Palette.name((red or 0.0, green or 0.0, blue or 0.0))

        if self._state['on']:
            self._display.fill(_Overlay.light, Palette.rgbf(self._state['color']))
        else:
            self._display.clear(_Overlay.light)

        if old_state != self._state:
            self.emit_event('state', self._state)

        return self.response(**self._state)

    def _start(self, argv):
        self._state = {'on': False, 'brightness': self.conf['light.brightness'], 'color': self.conf['light.color']}

        self._display.brightness = self._state['brightness']
        self._display.set(_Overlay.status, _LOGO, 'CornFlowerBlue')
        threading.Timer(2.0, self._display.clear).start()

        # Init disk health.
        h = self.send_command(SDISK, 'health')
        if h and h.error() is None:
            for name in ('hd', 'bk'):
                if name in h:
                    self._disk_health.update(h[name])

    def _stop(self):
        self._display.clear()


if __name__ == '__main__':
    _LightService().start(sys.argv)
