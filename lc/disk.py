# Copyright 2020 info@un8s.com
# License: MIT, see LICENSE for details.

import collections
import datetime
import enum
import os
import pathlib
import re
import signal
import subprocess
import sys
import threading
import time

import psutil

from lib import log
from lib.srv import ServiceDef, Service


class _StartMode(enum.IntEnum):
    """Disk start modes and their priority."""
    none = 0
    backup =  1
    auto = 2
    ondemand = 3
    idle = 3
    shutdown = 4

def _check_wake_periods(value):
    if not isinstance(value, list):
        raise ValueError(f"Value for 'wake.periods' must be a list of (<start day>, <start time>, <end day>, <end time>): {value}")
    for p in value:
        if not isinstance(p, set) or len(p) != 4:
            raise ValueError(f"Value for 'wake.periods' must be a list of (<start day>, <start time>, <end day>, <end time>): {value}")
        (start_d, start_t, end_d, end_t) = p
        if not (0 <= start_d <= 6) or not (0 <= end_d <= 6):
            raise ValueError(f"Value for start or end day of 'wake.periods' must be between 0 (Monday) and 6 (Sunday): {value}")
        if not (0 <= start_t <= 23) or not (0 <= end_t <= 23):
            raise ValueError(f"Value for start or end time of 'wake.periods' must be between 0 and 23: {value}")
    return value

def _check_idle_timeout(value):
    if not (0 <= value):
        raise ValueError(f"Value for 'idle.timeout' must be positive: {value}")
    return value

SDEF = ServiceDef({
    'app': 'lc',
    'name': 'disk',
    'version': '20.12',
    'description': 'Disk control and monitoring service.',
    'checkuid': 0,
    'mbus': { 'broker.name': '_lc' },
    'conf': {
        'hd.mount': {
            'value': '/mnt/hd',
            'type': str,
            'required': True,
            'readonly': True,
            'internal': True
        },
        'bk.mount': {
            'value': '/mnt/bk',
            'type': str,
            'required': True,
            'readonly': True,
            'internal': True
        },
        'sys.mount': {
            'value': '/',
            'type': str,
            'required': True,
            'readonly': True,
            'internal': True
        },
        'wake.periods': {
            'value': [(0, 6, 15, 16)],
            'check': _check_wake_periods
        },
        'idle.timeout': {
            'value': 30,
            'type': int,
            'check': _check_idle_timeout
        }
    },
    'commands': {
        'start': {
            'args': {
                'name': {
                    'type': str,
                    'required': True,
                    'choices': ['bk', 'hd']
                },
                'mode': {
                    'type': str,
                    'choices': list(_StartMode.__members__),
                    'default': _StartMode.ondemand.name
                }
            }
        },
        'stop': {
            'args': 'start'
        },
        'toggle': {
            'args': 'start'
        },
        'health': {
            'args': {
                'name': {
                    'type': str,
                    'choices': ['bk', 'hd', 'sys']
                }
            }
        }
    },
    'events': {
        'emit': {
            '(hd|bk)\.state': {
                'args': {
                    'name': { 'type': str },
                    'started': { 'type': bool },
                    'mode': { 'type': _StartMode }
                }
            },
            '(hd|bk)\.access': {
                'args': {
                    'name': { 'type': str },
                    'write_access': { 'type': bool }
                }
            },
            'hd.shares': {
                'args': {
                    'name': { 'type': str },
                    'shares': { 'type': dict }
                }
            },
            '(hd|bk)\.health': {
                'args': {}
            }
        }
    },
    'schedules': {
        'every_hour': {
            'minute': 0,
            'init': True
        },
        'every_60_minutes': {
            'interval': 60
        },
        'every_10_minutes': {
            'interval': 10,
            'init': True
        },
        'every_five_seconds': {
            'interval': 0.05
        }
    },
})

# TODO Ask backup for mounted archives before stopping bk disk. In "ondemand" mode, ask backup to unmount all.

# Samba audit log that has been configured as follows:
#   vfs objects = catia fruit streams_xattr full_audit
#   log level = 2 vfs:2
#   logging = syslog
#   full_audit:prefix = %u|%m
#   full_audit:success = connect disconnect
#   full_audit:failure = none
#   full_audit:facility = local0
#   full_audit:priority = info
#
# Rsyslog configuration /etc/rsyslog.conf:
#   local0.*                        -/run/user/1000/smbd_vfs.log
#
# Sample log entries:
#   Nov  8 01:40:50 lightcapsule smbd_audit: tobias|macbookpro13|connect|ok|timemachine
#   Nov  8 01:40:50 lightcapsule smbd_audit: tobias|macbookpro13|disconnect|ok|timemachine
_SAMBA_AUDIT_LOG = '/run/user/1000/smbd_vfs.log'

class _Samba:
    """Control Samba."""

    def is_started(self):
        """Return True is smbd is running."""
        sc = subprocess.run(['/bin/systemctl', 'is-active', 'smbd'], stdout=subprocess.DEVNULL)
        return sc.returncode == 0

    def start(self):
        """Start smbd (if necessary.)"""
        if not self.is_started():
            log.debug('Starting samba...')
            subprocess.run(['/bin/systemctl', 'start', 'smbd'], check=True)

    def stop(self):
        """Stop smbd (if necessary.)"""
        if self.is_started():
            log.debug('Stopping samba...')
            subprocess.run(['/bin/systemctl', 'stop', 'smbd'])

    def get_connected(self):
        """Return dict of currently connected shares."""
        sh = {'share': {'user': [], 'host': []}, 'timemachine': {'user': [], 'host': []}}
        if self.is_started():
            try:
                with open(_SAMBA_AUDIT_LOG) as f:
                    f.seek(0, os.SEEK_END)
                    file_size = f.tell()
                    f.seek(max(0, file_size-(2*1024)))
                    vfs_log = f.readlines()
                if len(vfs_log):
                    for line in vfs_log:
                        try:
                            smb = line.strip().split(':')
                            user, host, action, state, share = smb[-1].strip().split('|')
                            if action == 'connect' and state == 'ok':
                                sh[share]['user'].append(user)
                                sh[share]['host'].append(host)
                            elif action == 'disconnect' and state == 'ok':
                                sh[share]['user'].remove(user)
                                sh[share]['host'].remove(host)
                        except Exception as ex:
                            log.debug(ex)  # Ignore error in first line which may be incomplete.
            except Exception as ex:
                log.debug(ex)  # File not available.
        csh = {}
        for share in sh.keys():
            sh[share]['user'] = set(sh[share]['user'])
            sh[share]['host'] = set(sh[share]['host'])
            if len(sh[share]['user']):
                assert (sh[share]['host']), sh
                csh[share] = sh[share]
        return csh

    def has_connected(self):
        """Return True if there are currently connected shares."""
        csh = self.get_connected()
        return ('share' in csh and len(csh['share']['user']) > 0) or ('timemachine' in csh and len(csh['timemachine']['user']) > 0)


_CachedDiskUsage = collections.namedtuple('_CachedDiskUsage', ['total', 'used', 'free', 'percent'])

class _Disk:
    """Control disk."""

    def __init__(self, name, mount, srv):
        assert name in ('hd', 'bk', 'lc', 'bc')
        self._name = name
        self._srv = srv
        self._mount = mount
        self._device = None
        self._health = {}
        self._stat = {'read_count': 0, 'write_count': 0, 'write_bytes': 0}
        self._read_access, self._write_access = None, None
        self._last_access = time.time()

    @property
    def name(self):
        return self._name

    @property
    def write_bytes(self):
        return self._stat['write_bytes']

    @property
    def read_access(self):
        return self._read_access

    @property
    def write_access(self):
        return self._write_access

    def is_accessed(self):
        return self._read_access or self._write_access

    @property
    def last_access(self):
        return self._last_access

    @property
    def device(self):
        """Mounted device name (without /dev/)"""
        if self._device is None:
            mounted = [p.device for p in psutil.disk_partitions() if p.mountpoint == self._mount]
            assert len(mounted)
            self._device = mounted[0][len('/dev/'):]
        return self._device

    def is_started(self, read_only=None):
        """Return True if disk is mounted (and mounted in given mode)."""
        opt = 'ro' if read_only else 'rw'
        mounted = [p.mountpoint for p in psutil.disk_partitions() if read_only is None or opt in p.opts.split(',')]
        return self._mount in mounted

    def get_health(self):
        """Return dict with health data."""
        self._health[f'{self.name}.started'] = self.is_started()
        self._health[f'{self.name}.write_access'] = None if not self.is_accessed() else self._write_access
        return dict(self._health)

    def update_access(self, read_count, write_count, write_bytes):
        """Return (read_access, write_access) after updating access data."""
        self._read_access = read_count != self._stat['read_count']
        self._write_access = write_count != self._stat['write_count']
        self._stat['read_count'], self._stat['write_count'] = read_count, write_count
        self._stat['write_bytes'] = write_bytes
        if self.is_accessed():
            self._last_access = time.time()
        return self._read_access, self._write_access

    def update_usage(self):
        """Update usage data in health dict. If disk is stopped, get values from cache."""
        if self.is_started():
            du = psutil.disk_usage(self._mount)
            self._srv.store_value(f'{self.name}.du', {'total': du.total, 'used': du.used, 'free': du.free, 'percent': du.percent})
        else:
            cdu = self._srv.read_value(f'{self.name}.du')
            if cdu is None:
                return
            du = _CachedDiskUsage(**cdu)
        log.debug(f'du({self.name}): {du}')
        self._health[f'{self.name}.total'] = du.total
        self._health[f'{self.name}.used'] = du.used
        self._health[f'{self.name}.free'] = du.free
        self._health[f'{self.name}.load'] = du.percent


class _MountableDisk(_Disk):
    """Control disk that can be started/stopped."""

    def __init__(self, name, mount, srv):
        assert self.__class__ != _MountableDisk, 'Abstract class'
        _Disk.__init__(self, name, mount, srv)
        self._start_mode = _StartMode.none

    @property
    def start_mode(self):
        return self._start_mode

    def start(self, mode):
        """Power on and mount disk (if necessary.)"""
        if isinstance(mode, str):
            mode = _StartMode[mode]
        if mode > self._start_mode:
            self._start_mode = mode
        if not self.is_started():
            log.mount(f'Mounting {self.name} in {mode.name} mode...')
            subprocess.run(['/bin/mount', self._mount])
        self._last_access = time.time()

    def stop(self):
        """Unmount disk and power it off (if necessary.)"""
        self._start_mode = _StartMode.none
        self._read_access, self._write_access = None, None
        if self.is_started():
            log.mount(f'Unmounting {self.name} ({self.device})...')
            subprocess.run(['/bin/umount', self._mount])
            subprocess.run(['/sbin/hdparm', '-y', f'/dev/{self.device[:-1]}'])

    def update_smart_info(self):
        """Update SMART info in health dict."""
        si = {}
        sc = subprocess.run(['/usr/sbin/smartctl', '-A', '-H', '-i', f'/dev/{self.device[:-1]}'], capture_output=True, text=True)
        in_attr = False
        for line in sc.stdout.splitlines():
            line = line.strip()
            if in_attr:
                a = line.split()
                if len(a):
                    # TODO Check all attributes of type 'Pre-fail'?
                    if a[1] == 'Temperature_Celsius':
                        si[f'{self.name}.temp'] = int(a[9])
            elif line.startswith('SMART support is:'):
                m = re.match('SMART support is: (Enabled|Disabled)', line)
                if m:
                    si[f'{self.name}.smart.enabled'] = m.group(1).lower() == 'enabled'
            elif line.startswith('SMART overall-health'):
                m = re.match('SMART overall-health self-assessment test result: (.*)', line)
                if m:
                    si[f'{self.name}.smart.self-check-passed'] = m.group(1).lower() == 'passed'
            elif line.startswith('ID#'):
                in_attr = True
        log.debug(f'SMART({self.name}): {si}')
        self._health.update(si)


class _BackupDisk(_MountableDisk): ...

class _SharesDisk(_MountableDisk):

    def __init__(self, name, mount, srv):
        _MountableDisk.__init__(self, name, mount, srv)
        self._shares_total_in_progress = False
        self._last_shares_total_update = 0

    def update_shares_total(self):
        """Update share totals in health dict."""
        passed = int((time.time() - self._last_shares_total_update) / 60)
        if not self.is_started() or self._shares_total_in_progress or passed < 30:
            st = self._srv.read_value(f'{self.name}.st')
            if st is not None:
                log.debug(f'st.cached({self.name}): {st}')
                self._health.update(st)
            return

        self._shares_total_in_progress = True
        try:
            st = {}
            for sh in [d.name for d in os.scandir(self._mount) if d.is_dir() and d.name != 'lost+found' and d.name[0] != '.']:
                p = pathlib.Path(os.path.join(self._mount, sh))
                st[f'{self.name}.{sh}.total'] = sum(f.stat().st_size for f in p.glob('**/*') if f.is_file())
            log.debug(f'st({self.name}): {st}')
            self._health.update(st)
            self._srv.store_value(f'{self.name}.st', st)
            self._last_shares_total_update = time.time()
        except FileNotFoundError:
            pass # This can happen if the disk content changes or the disk is stopped.
        finally:
            self._shares_total_in_progress = False


class _SysType(enum.Enum):
    LightCapsule = 'lc'
    BackupCompanion = 'bc'


class _DiskService(Service):
    """Disk control and monitoring service.

    Tasks:
    - Power hard disks on/off
    - Un-/mount hard disks
    - Control samba processes
    - Monitor hard disk use
    
    """

    def __init__(self):
        super().__init__(SDEF)

    def _start_disk(self, mode):
        """Return True if disk (and Samba) is started."""
        if isinstance(mode, str):
            mode = _StartMode[mode]
        was_started = self._disk.is_started()

        # Start disk if necessary and make sure it is started.
        self._disk.start(mode)
        retry = 10
        while not self._disk.is_started() and retry > 0:
            time.sleep(0.5)
            retry -= 1
        if not self._disk.is_started():
            return False
        self._disk.update_usage()
        self._disk.update_smart_info()

        # Start Samba if necessary and make sure it is started.
        if isinstance(self._disk, _SharesDisk) and mode != _StartMode.backup:
            self._samba.start()
            retry = 10
            while not self._samba.is_started() and retry > 0:
                time.sleep(0.5)
                retry -= 1
            if not self._samba.is_started():
                if not was_started and mode >= self._disk.start_mode:
                    self._disk.stop()
                return False

        assert self._disk.is_started()

        if isinstance(self._disk, _SharesDisk):
            self.schedule('+600', self._disk.update_shares_total)

        # Send event when disk state has changed.
        if not was_started:
            self.emit_event(f'{self._disk.name}.state', {'started': True, 'name': self._disk.name, 'mode': mode.name}, log=True)
        
        self.publish_health()
        log.info(f'{self._disk.name} started in mode {mode.name}.')
        return True

    def _stop_disk(self, mode):
        """Return True if disk (and Samba) is stopped."""
        if isinstance(mode, str):
            mode = _StartMode[mode]

        if mode < self._disk.start_mode:
            log.info(f'Not stopping {self._disk.name} which has been started in {self._disk.start_mode.name} mode.')
        elif self._disk.is_accessed():
            log.info(f'Not stopping {self._disk.name} which is currently accessed.')
        elif isinstance(self._disk, _SharesDisk) and self._samba.has_connected():
            log.info(f'Not stopping {self._disk.name} which has connected shares.')
        else:
            was_started = self._disk.is_started()

            # Stop Samba if necessary and make sure it is stopped.
            if isinstance(self._disk, _SharesDisk):
                self._samba.stop()
                retry = 10
                while self._samba.is_started() and retry > 0:
                    time.sleep(0.5)
                    retry -= 1
                if self._samba.is_started():
                    return False

            # Stop disk if necessary and make sure it is stopped.
            self._disk.stop()
            retry = 10
            while self._disk.is_started() and retry > 0:
                time.sleep(0.5)
                retry -= 1
            if self._disk.is_started():
                # TODO Re-start Samba at this point?
                return False

            assert not self._disk.is_started()

            # Send event when disk state has changed.
            if was_started:
                self.emit_event(f'{self._disk.name}.state', {'started': False, 'name': self._disk.name, 'mode': mode.name}, log=True)
                log.info(f'{self._disk.name} stopped in mode {mode.name}.')

        return self._disk.is_started()

    def _get_health(self, disk):
        """Return disk health. Enrich health of hd with samba info."""
        h = disk.get_health()
        if isinstance(disk, _SharesDisk):
            h[f'{disk.name}.shared'] = self._samba.is_started()
            h[f'{disk.name}.shares'] = self._samba.get_connected()
        return h

    def update_access(self, notify=True):
        all_counters = psutil.disk_io_counters(perdisk=True)
        disk = self._disk
        if disk.is_started():
            counters = all_counters.get(disk.device, None)
            if counters is not None:
                was_read_access, was_write_access = disk.read_access, disk.write_access
                read_access, write_access = disk.update_access(counters.read_count, counters.write_count, counters.write_bytes)
                if notify and ((read_access != was_read_access) or (write_access != was_write_access)):
                    self.emit_event(f'{disk.name}.access', {'name': disk.name, 'write_access': None if not (read_access or write_access) else write_access})
            # Also check shares.
            if isinstance(disk, _SharesDisk):
                csh = self._samba.get_connected()
                if self._connected_shares != csh:
                    self._connected_shares = dict(csh)
                    if notify:
                        self.emit_event(f'{disk.name}.shares', {'name': disk.name, 'shares': csh})

    def publish_health(self):
        for disk in (self._disk, self._sys):
            h = self._get_health(disk)
            if self._published_health[disk] != h:
                self._published_health[disk] = h
                self.emit_event(f'{disk.name}.health', h)

    def every_hour(self):
        """This is called every hour."""
        assert self._type == _SysType.LightCapsule, self._type

        # Check if hd needs to be started.
        dt = datetime.datetime.now()
        in_period = False
        if self.conf['wake.periods'] is not None:
            for first_day, last_day, begin_hour, end_hour in self.conf['wake.periods']:
                if first_day <= dt.weekday() <= last_day and begin_hour <= dt.hour < end_hour:
                    log.debug(f'In wake period {first_day}-{last_day} {begin_hour}-{end_hour}')
                    in_period = True
                    break

        if in_period:
            self._start_disk(_StartMode.auto)
        else:
            self._stop_disk(_StartMode.auto)

    def every_60_minutes(self):
        """This is called every 60 minutes."""
        assert self._type == _SysType.LightCapsule, self._type
        self._disk.update_shares_total()

    def every_10_minutes(self):
        """This is called every 10 minutes."""

        # Update health.
        for disk in (self._disk, self._sys):
            disk.update_usage()

        disk = self._disk
        if disk.is_started():
            # Update health.
            disk.update_smart_info()

            # Check idleness.
            idle = int((time.time() - disk.last_access) / 60)
            log.debug(f'{disk.name} is idle for {idle} minutes.')
            if self.conf['idle.timeout'] is not None and idle >= self.conf['idle.timeout']:
                if disk.start_mode in (_StartMode.auto,):
                    log.info(f'Not stopping idle {disk.name} which has been started in {disk.start_mode.name} mode.')
                else:
                    self._stop_disk(_StartMode.idle)

        # Publish health updates.
        self.publish_health()

    def every_five_seconds(self):
        """Publish access updates."""
        self.update_access()

    def do_start(self, name, mode):
        if name != self._disk.name:
            return self.response()
        self._start_disk(mode)
        return self.response(**self._get_health(self._disk))

    def do_stop(self, name, mode):
        """Return True if disk (and Samba) is stopped."""
        if name != self._disk.name:
            return self.response()
        self._stop_disk(mode)
        return self.response(**self._get_health(self._disk))

    def do_toggle(self, name, mode):
        return self.do_stop(name, mode) if self._disk.is_started() else self.do_start(name, mode)

    def do_health(self, name):
        if name is not None and name not in (self._disk.name, 'sys'):
            return self.response()
        if name is None:
            return self.response(**{disk.name: self._get_health(disk) for disk in (self._disk, self._sys)})
        else:
            return self.response(**self._get_health(self._sys if name == 'sys' else self._disk))

    def _start(self, argv):
        """Running as LightCapsule or BackupCompanion."""

        if len(argv) > 1 and argv[1] == '--lc':
            self._type = _SysType.LightCapsule
            self._sys = _Disk(self._type.value, self.conf['sys.mount'], self)
            self._disk = _SharesDisk('hd', self.conf['hd.mount'], self)
            self._samba = _Samba()
            self._connected_shares = {}

        elif len(argv) > 1 and argv[1] == '--bc':
            self._type = _SysType.BackupCompanion
            self._sys = _Disk(self._type.value, self.conf['sys.mount'], self)
            self._disk = _BackupDisk('bk', self.conf['bk.mount'], self)
            del self._sd.schedules['every_hour']
            del self._sd.schedules['every_60_minutes']

        else:
            sys.exit(f'usage: {argv[0]} --lc|--bc')

        self._published_health = {self._sys: {}, self._disk: {}}
        self.update_access(notify=False)

    def _stop(self):
        """Shutdown disk when stopping service."""
        self._stop_disk(_StartMode.shutdown)


if __name__ == '__main__':
    _DiskService().start(sys.argv)
