# Copyright 2020 info@un8s.com
# License: MIT, see LICENSE for details.

import bz2
import datetime
import io
import json
import os
import subprocess
import sys
import time

import psutil

from lib import log, timestamp
from lib.srv import ServiceAddress, ServiceDef, Service

from disk import SDEF as SDISK

# TODO Determine typical system idle time and schedule backup accordingly?
# TODO Unmount idle archives?

def _check_backup_start(value):
    if not (0 <= value <= 23):
        raise ValueError(f"Value for 'backup.start' must be between 0 and 23: {value}")
    return value

SDEF = ServiceDef({
    'app': 'lc',
    'name': 'backup',
    'version': '20.12',
    'description': 'Backup service.',
    'checkuid': 0,
    'mbus': { 'broker.name': '_lc' },
    'conf': {
        'hd.mount': SDISK.conf_def('hd.mount'),
        'backup.start': {
            'value': 3,
            'type': int,
            'check': _check_backup_start
        },
        'backup.paused': {
            'value': True,
            'type': bool
        }
    },
    'commands': {
        'list': {
            'args': {
                'share': { 'type': str, 'choices': ['share', 'timemachine'] },
                'cache': { 'type': bool, 'default': False },
                'info': { 'type': bool, 'default': False }
            }
        },
        'info': {
            'args': {
                'archive': { 'type': str, 'required': True },
                'cache': { 'type': bool, 'default': False },
                'ls': { 'type': bool, 'default': False },
                'compressed': { 'type': bool, 'default': True }
            }
        },
        'create': {
            'args': {
                'share': { 'type': str, 'required': True, 'choices': ['share', 'timemachine'] }
            }
        },
        'mount': {
            'args': {
                'archive': { 'type': str, 'required': True },
                'compressed': { 'type': bool, 'default': True }
            }
        },
        'umount': {
            'args': {
                'archive': { 'type': str, 'required': True }
            }
        },
        'delete': {
            'args': {
                'archive': { 'type': str, 'required': True }
            }
        },
        'prune': {}
    },
    'events': {
        'emit': {
            'backup\.(create|list|info|mount|umount|delete|prune)\.start': {
                'args': {
                    'name': { 'type': str }
                }
            },
            'backup\.(create|list|info|mount|umount|delete|prune)\.end': {
                'args': {
                    'name': { 'type': str },
                    'rc': { 'type': int },
                    'error': { 'type': str }
                }
            },
        }
    },
    'schedules': {
        'every_hour': {
            'minute': 0,
            'init': True
        }
    }
})

class _UnavailableError(RuntimeError): ...
class _BorgError(RuntimeError): ...

_bs = None

def resolve_repo():
    """Return URI to repository. Raise _UnavailableError if BackupCompanion is not available."""
    bcs = ServiceAddress.find('_lc-bc')
    log.debug(f'Looking for BackupCompanion: {bcs}')
    if bcs is None:
        raise _UnavailableError('Cannot reach BackupCompanion')
    return f'{bcs.user}@{bcs.address}:{bcs.repo}'

def call_borg(command, *args, text=True, out='stdout', persist=False):
    """Return stdout after calling borg with command and args. Raise _BorgError if borg
    process did not terminate successfully."""
    args = list(args)
    name = None
    for index, arg in enumerate(args):
        if (arg.startswith('share-') or arg.startswith('timemachine-')) and arg not in ('share-', 'timemachine-'):
            name = arg
            args[index] = f'{resolve_repo()}::{name}'
    edata = {'name': name}
    _bs.emit_event(f'backup.{command}.start', edata, log=persist)
    log.debug(f'/usr/bin/borg {command} {args}')
    bp = subprocess.run(['/usr/bin/borg', command, *args],
            capture_output=True, text=text, env={
                'BORG_BASE_DIR': f'{_bs.conf["hd.mount"]}/.borg',
                'BORG_UNKNOWN_UNENCRYPTED_REPO_ACCESS_IS_OK': 'yes',
                'BORG_RELOCATED_REPO_ACCESS_IS_OK': 'yes'}  # because IP address may change
    )
    edata['rc'] = bp.returncode
    if bp.returncode != 0:
        edata['error'] = bp.stderr.strip() if text else bp.stderr.decode('utf-8').strip()
    log.debug(edata)
    _bs.emit_event(f'backup.{command}.end', edata, log=persist)
    if bp.returncode == 0:
        return bp.stdout if out == 'stdout' else bp.stderr
    raise _BorgError(edata['error'])


class _Archive:
    """Archive."""

    @classmethod
    def create(cls, share):
        """Return Archive after creating it. Raise _BorgError."""
        dt = datetime.datetime.now()
        a = cls(f'{share}-{dt.strftime("%Y-%m-%d-%H")}')
        a._create()
        return a

    @classmethod
    def find(cls, name):
        """Return Archive if it exists, else None."""
        a = cls(name)
        return a if a.exists() else None

    @classmethod
    def list(cls, share=None):
        """Return dict with all available archives per share sorted by newest first.
        Return list of all available archives for specified share sorted by newest first.
        Raise _BorgError."""
        assert share is None or share in ('share', 'timemachine'), share
        shares = ('share', 'timemachine') if share is None else (share,)
        archives = {'share': [], 'timemachine': []}        
        for line in call_borg('list', resolve_repo()).splitlines():
            name, _, date, time, _ = line.split()
            archive_source = name.split('-')[0]
            if archive_source in shares:
                archives[archive_source].append(name)
        for sa in archives.values():
            sa.sort(reverse=True)
        return archives if share is None else archives[share]

    @classmethod
    def list_cache(cls, share=None, info=False):
        """Return dict with all cached archives per share sorted by newest first.
        Return list of all cached archives for specified share sorted by newest first."""
        archives = {'share': [], 'timemachine': []}        
        for k in _bs.find_value('info-.*'):
            name = k[len('info-'):]
            if info:
                archives[name.split('-')[0]].append(_Archive(name).read_info_cache(ls=False))
            else:
                archives[name.split('-')[0]].append(name)
        for sa in archives.values():
            if info:
                sa.sort(key=lambda k: k['name'], reverse=True)
            else:
                sa.sort(reverse=True)
        return archives if share is None else archives[share]

    def __init__(self, name):
        self._name = name
        self._share = name.split('-')[0]
        self._mountpoint = f'/mnt/{name}'
        self._mounted_since = None
        self._mounted_count = 0

    @property
    def name(self):
        return self._name

    @property
    def mountpoint(self):
        return self._mountpoint

    @property
    def mounted_since(self):
        return self._mounted_since

    @property
    def mounted_count(self):
        return self._mounted_count

    def _create(self):
        """Create archive. Raise _BorgError."""
        call_borg('create', '--compression=lz4', self._name, f'{_bs.conf["hd.mount"]}/{self._share}', persist=True)

        # Cache archive info, including a listing of archive's content.
        _bs.store_value(f'info-{self._name}', self.read_info(ls=True, compressed=True))

    def read_info_cache(self, ls=True, compressed=True):
        """Return cached archive info, or None."""
        cai = _bs.read_value(f'info-{self._name}')
        if cai is not None:
            if ls and not compressed:
                cai['ls'] = bz2.decompress(cai.pop('ls-bz2')).decode('utf-8')
            elif not ls:
                cai.pop('ls-bz2')
        return cai

    def read_info(self, ls=False, compressed=True):
        """Return archive info. Raise _BorgError."""
        borg_info = json.loads(call_borg('info', '--json', self._name))
        info = {'name': self._name, 'share': self._share, **{k: borg_info['archives'][0]['stats'][k] for k in ('nfiles', 'original_size', 'compressed_size')}}

        if ls:
            # Input format from borg (sorted by name, but not grouped by directory):

            # d 0 Fri, 2020-11-06 22:36:42 mnt/hd/share
            # - 12292 Fri, 2020-11-06 22:14:58 mnt/hd/share/.DS_Store
            # d 0 Wed, 2020-07-29 07:22:33 mnt/hd/share/Media
            # d 0 Sat, 2020-10-24 23:15:57 mnt/hd/share/Media/Movies
            # - 683354112 Mon, 2007-11-26 19:03:12 mnt/hd/share/Media/Movies/Overboard [1987] [SD].avi
            # - 368 Mon, 2020-10-26 20:11:01 mnt/hd/share/Media/Movies/._Overboard [1987] [SD].avi

            # Produce output format: {type=d|-} {size} {date} {time} {name}
            #   - strip mount point
            #   - group by directory
            #   - strip directory name from files

            # d - - - /Media/Movies
            # - 683354112 2007-11-26 19:03:12 Overboard [1987] [SD].avi

            prefix = f'{_bs.conf["hd.mount"][1:]}/{self._share}'
            lsd = {}
            for line in call_borg('list', '--format={type} {size} {mtime} {bpath}{NL}', self._name).splitlines():
                if len(line.strip()) == 0:
                    continue
                typ, size, _, date, time, bpath = line.split(maxsplit=5)
                bpath = bpath[len(prefix):]
                if typ == 'd':
                    lsd['/' if bpath == '' else bpath] = []
                    continue
                dir_name, name = os.path.split(bpath)
                lsd[dir_name].append(f'{typ} {size} {date} {time} {name}')

            with io.StringIO() as lso:
                for dir_name, dir_list in lsd.items():
                    lso.write(f'd - - - {dir_name}\n')
                    lso.write('\n'.join(dir_list))
                    lso.write('\n')

                if compressed:
                    info['ls-bz2'] = bz2.compress(lso.getvalue().encode('utf-8'))
                else:
                    info['ls'] = lso.getvalue()

        return info

    def exists(self):
        """Return True if archive exists."""
        try:
            self.read_info()
            return True
        except _BorgError:
            return False

    def is_mounted(self):
        """Return True if archive is mounted."""
        all = [p.mountpoint for p in psutil.disk_partitions(all=True) if p.device == 'borgfs']
        return self._mountpoint in all

    def mount(self):
        """Return True if archive was mounted successfully."""
        self._mounted_since = timestamp()
        self._mounted_count += 1
        if not self.is_mounted():
            try:
                os.mkdir(self._mountpoint)
            except Exception as ex:
                log.debug(ex)
            call_borg('mount', self._name, self._mountpoint)
        return self.is_mounted()

    def umount(self, force=False):
        """Return False if archive was unmounted successfully."""
        self._mounted_count = 0 if force else max(0, self._mounted_count-1)
        if self.is_mounted() and self._mounted_count == 0:
            self._mounted_since = None
            call_borg('umount', self._mountpoint)
        if not self.is_mounted():
            try:
                os.rmdir(self._mountpoint)
            except Exception as ex:
                log.debug(ex)
        return self.is_mounted()

    def delete(self):
        """Delete archive."""
        self.umount(force=True)
        call_borg('delete', '--force', '--save-space', self._name, persist=True)
        _bs.delete_value(f'info-{self._name}')

    def __str__(self):
        return f'Archive({self.name}, ts={self.mounted_since}, count={self.mounted_count})'


class _BackupService(Service):
    """Backup service."""

    def __init__(self):
        super().__init__(SDEF)
        self._mounted_archives = []

    def start_disk(self, name):
        """Return True if disk is running after requesting it to be started."""
        assert name in ('hd', 'bk'), name
        h = self.send_command(SDISK, 'start', name=name, mode='backup')
        return h and f'{name}.started' in h and h[f'{name}.started']

    def create_archive(self, share, mode=None):
        """Return dict with archive info after creating a new archive for the share."""
        assert share in ('share', 'timemachine'), share
        assert mode in ('auto', 'ondemand'), mode
        log.info(f'Backup share {share} in {mode} mode...')
        archive = _Archive.create(share)
        log.info(f'  {archive}')
        return archive.read_info_cache(ls=False)

    def prune(self):
        """Return list of pruned archives."""
        pruned = []
        for share in ('share', 'timemachine'):
            log.info(f'Pruning {share}...')
            report = call_borg('prune', '--list', '--prefix', f'{share}-',
                    '--keep-daily', '7', '--keep-weekly', '4', '--keep-monthly', '6', resolve_repo(), out='stderr')
            log.info(report)

            # Keeping archive: share-2020-11-16-17                  Mon, 2020-11-16 17:16:55 [586b49ed6cef17e2df459f1e7942acc98f2392417ab4e4b5e5e0f37f41ebd027]
            # Pruning archive: share-2020-11-16-16                  Mon, 2020-11-16 16:25:51 [3b7f8f5bd0c91b80ffd505abe6a7cb193fd2ce2c12f262bc76289df0b8344749] (1/1)

            for line in report.splitlines():
                action, _, name, *_ = line.split()
                if action in ('Pruning',):
                    pruned.append(name)

            # Sync cache with actual archives.
            actual = _Archive.list(share)
            for name in _Archive.list_cache(share):
                if name not in actual:
                    self.delete_value(f'info-{name}')

        return pruned

    def mount_archive(self, name, compressed=True):
        """Return archive and mount info after mounting archive."""
        log.info(f'Mounting {name}...')
        for archive in self._mounted_archives:
            if archive.name == name:
                break
        else:
            archive = _Archive.find(name)
            if archive is None:
                return {'error': f'Archive {name} does not exist.'}
            self._mounted_archives.append(archive)
        mounted = archive.mount()
        log.info(f'  {archive}')
        return {
            **archive.read_info_cache(ls=True, compressed=compressed),
            'mounted': mounted, 'mountpoint': archive.mountpoint, 'count': archive.mounted_count
        }

    def umount_archive(self, name, force=False):
        """Return archive and mount info after unmounting archive. If force is True, unmount archive even if count is not zero."""
        log.info(f'Unmounting {name} (force={force})...')
        for archive in self._mounted_archives:
            if archive.name == name:
                break
        else:
            archive = None
        mounted = archive.umount(force) if archive is not None else None
        if mounted is False:
            self._mounted_archives.remove(archive)
        log.info(f'  {archive}')
        return {'name': name, 'mounted': mounted, 'count': None if archive is None else archive.mounted_count}

    def every_hour(self):
        """Triggered once every hour."""

        # Check time.
        dt = datetime.datetime.now()
        backup_start = _bs.conf['backup.start']
        if backup_start is None:
            log.debug(f'Skip backup: no schedule.')

        elif not (backup_start <= dt.hour < backup_start+1):
            log.debug(f'Skip backup: outside schedule ({backup_start}-{backup_start+1}).')

        elif _bs.conf['backup.paused']:
            log.debug(f'Skip backup: paused.')

        else:
            def _report(r):
                self.emit_event('backup.result', r, alert='error' in r, log=True)

            # Request that disks are on.
            if not self.start_disk('hd'):
                _report({'error': 'Failed to start shares disk.', 'mode': 'auto'})
            elif not self.start_disk('bk'):
                _report({'error': 'Failed to start backup disk.', 'mode': 'auto'})
            else:
                for share in ('share', 'timemachine'):
                    try:
                        _report(self.create_archive(share, mode='auto'))
                    except _BorgError as ex:
                        _report({'error': str(ex)})

            self.prune()

    def _do_command(self, command, args):
        log.debug(f'Do {command}({args})')

        # These commands return cached info only and therefore don't require disk access.

        if args.get('cache', False):

            if command == 'list':
                return self.response(**_Archive.list_cache(args.share, info=args.info))

            if command == 'info':
                info = _Archive(args.archive).read_info_cache(args.ls, args.compressed)
                return self.response(**info) or self.error_response(f'Archive {args.archive} does not exist.')

        # All commands below require disk access.

        if not self.start_disk('bk'):
            return self.error_response('Failed to start backup disk.', mode='ondemand')
        if not self.start_disk('hd'):
            return self.error_response('Failed to start shares disk.', mode='ondemand')

        try:
            if command == 'list':
                return self.response(**_Archive.list(args.share))

            if command == 'info':
                return self.response(**_Archive(args.archive).read_info(args.ls, args.compressed))

            if command == 'create':
                return self.response(**self.create_archive(args.share, mode='ondemand'))

            if command == 'mount':
                return self.response(**self.mount_archive(args.archive, args.compressed))

            if command == 'umount':
                return self.response(**self.umount_archive(args.archive))

            if command == 'delete':
                _Archive(args.archive).delete()
                return self.response(archive=args.archive)

            if command == 'prune':
                return self.response(pruned=self.prune())

        except _BorgError as ex:
            return self.error_response(str(ex))

    def _stop(self):
        for ma in self._mounted_archives:
            ma.umount(force=True)


if __name__ == '__main__':
    _bs = _BackupService()
    _bs.start(sys.argv)
