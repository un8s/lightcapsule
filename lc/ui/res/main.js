// window.setInterval("refreshDiv()", 10000);  
// function refreshDiv() {
//     document.getElementById("disks").innerHTML = "";
// }

function update_ui_status(text, status) {
    if (text == "_reload") {
        location.reload();
    }
    if (status == "is-danger")
        sse.close();
    b = document.getElementById("ui-status-button")
    b.classList.remove("is-success");
    b.classList.remove("is-warning");
    b.classList.remove("is-danger");
    b.classList.add(status);
}

function url(path) {
    return path + "?sid=" + document.head.id;
}

function show_spinner(show) {
    if (show) {
        document.getElementById("spinner").classList.remove("is-hidden");
        document.getElementById("curtain").classList.remove("is-hidden");
    } else {
        document.getElementById("spinner").classList.add("is-hidden");
        document.getElementById("curtain").classList.add("is-hidden");
    }
}

function switch_to_tab(tab) {
    show_spinner(true);

    function set_tab_content(content) {
        if (content == "_reload") {
            location.reload();
        }

        document.getElementById("tab-container").innerHTML = content;

        // Load tab specific script after installing tab content.
        var script = document.createElement("script");
        script.id = tab + "-script";
        script.src = "/res/" + tab + ".js";
        script.onload = function() {
            // Remove script immedialtely after loading.
            document.head.removeChild(this);
        };
        document.head.appendChild(script);

        show_spinner(false);
    }
    
    // Update tab nav.
    for (t of ["status", "backup", "settings"]) {
        document.getElementById(t + "-tab").classList.remove("is-active");
    }
    document.getElementById(tab + "-tab").classList.add("is-active");

    // Fetch tab content.
    fetch(url("/" + tab))
        .then(response => response.text())
        .then(text => set_tab_content(text));
}

// Server-side events.

var sse = new EventSource(url("/sse"));
sse.onerror = function (err) {
    update_ui_status("offline", "is-warning");
};

sse.addEventListener('alerts', function (e) {
    document.getElementById("sse-alert-content").innerHTML = e.data;
    var alert = document.getElementById("sse-alert");
    if (e.data == "")
        alert.classList.add("is-hidden");
    else
        alert.classList.remove("is-hidden");
}, false);

window.onload = function () {
    console.log(document.head.id);

    if (!window.EventSource) {
        console.log("Server-side events are not supported :-(");
    }
    if (!window.fetch) {
        console.log("Fetch API is not supported :-(");
    }

    // Status button and shutdown commands.

    document.getElementById("ui-status-dropdown").onclick = function () {
        document.getElementById("ui-status-dropdown").classList.toggle('is-active');
    };
    document.getElementById("a-shutdown").onclick = function () {
        fetch(url("/do/halt/now"))
            .then(response => response.text())
            .then(text => update_ui_status(text, "is-danger"));
    };
    document.getElementById("a-reboot").onclick = function () {
        fetch(url("/do/reboot/now"))
            .then(response => response.text())
            .then(text => update_ui_status(text, "is-danger"));
    };

    // Tab nav.

    document.getElementById("status-tab").onclick = function () {
        switch_to_tab("status");
    };
    document.getElementById("backup-tab").onclick = function () {
        switch_to_tab("backup");
    };
    document.getElementById("settings-tab").onclick = function () {
        switch_to_tab("settings");
    };

    // Always start with Status tab.

    switch_to_tab("status");
}
