// Disk power buttons.

function action_return(text) {
    document.body.style.cursor = 'default';
    if (text == "_reload") {
        location.reload();
    }
}

document.getElementById("a-hd-power").onclick = function () {
    document.body.style.cursor = 'wait';
    fetch(url("/do/power/hd"))
    .then(response => response.text())
    .then(text => action_return(text));
};

document.getElementById("a-bk-power").onclick = function () {
    document.body.style.cursor = 'wait';
    fetch(url("/do/power/bk"))
    .then(response => response.text())
    .then(text => action_return(text));
};

document.getElementById("a-disks-title").onclick = function () {
    document.getElementById("sse-disks-content").classList.toggle("is-hidden");
};
document.getElementById("a-system-title").onclick = function () {
    document.getElementById("sse-system-content").classList.toggle("is-hidden");
};
document.getElementById("a-log-title").onclick = function () {
    document.getElementById("sse-log-content").classList.toggle("is-hidden");
};

// Log reload button.

document.getElementById("a-log-reload").onclick = function () {
    document.body.style.cursor = 'wait';
    fetch(url("/do/reload/log"))
    .then(response => response.text())
    .then(text => action_return(text));
};

// Server-side events to update content.

sse.addEventListener('status', function (e) {
    update_ui_status("online", "is-success");

    if (!document.getElementById("sse-system-time"))
        return;

    try {
        var sse_data = JSON.parse(e.data);

        // System: time and load.
        document.getElementById("sse-system-load").innerHTML = sse_data["lc.cpu.load"] + " / " + sse_data["bc.cpu.load"];
        document.getElementById("sse-system-temp").innerHTML = sse_data["lc.cpu.temp"] + " / " + sse_data["bc.cpu.temp"];
        document.getElementById("sse-system-time").innerHTML = sse_data["lc.time"];
        
        // Disks: shares disk.
        var hd_state = document.getElementById("sse-hd-state");
        hd_state.classList.remove("has-background-success");
        hd_state.classList.remove("has-background-success-light");
        hd_state.classList.remove("has-background-danger-light");
        if (sse_data["hd.access"] == "true") 
            hd_state.classList.add("has-background-success");
        else if (sse_data["hd.state"] == "on")
            hd_state.classList.add("has-background-success-light");
        else
            hd_state.classList.add("has-background-danger-light");

        for (sh of ["share", "timemachine"]) {
            var connected = document.getElementById("sse-" + sh + "-connected");
            if (connected) 
                if (sse_data["hd." + sh] == "true")
                    connected.classList.add("has-background-success");
                else
                    connected.classList.remove("has-background-success");
        }
                        
        // Disks: backup disk.
        var bk_state = document.getElementById("sse-bk-state");
        bk_state.classList.remove("has-background-success");
        bk_state.classList.remove("has-background-warning-light");
        bk_state.classList.remove("has-background-success-light");
        bk_state.classList.remove("has-background-danger-light");
        if (sse_data["bk.online"] == "false")
            bk_state.classList.add("has-background-warning-light");
        else if (sse_data["bk.access"] == "true") 
            bk_state.classList.add("has-background-success");
        else if (sse_data["bk.state"] == "on")
            bk_state.classList.add("has-background-success-light");
        else
            bk_state.classList.add("has-background-danger-light");
    } catch (err) {
        console.log(err);
    }
}, false);

sse.addEventListener('disks', function (e) {
    try {
        var content = document.getElementById("sse-disks-content");
        if (content)
            content.innerHTML = e.data
    } catch (err) {
        log.console(err)
    }
}, false);

sse.addEventListener('system', function (e) {
    try {
        var content = document.getElementById("sse-system-content");
        if (content)
            content.innerHTML = e.data
    } catch (err) {
        log.console(err)
    }
}, false);

sse.addEventListener('log', function (e) {
    try {
        var content = document.getElementById("sse-log-content");
        if (content)
            content.innerHTML = e.data
    } catch (err) {
        log.console(err)
    }
}, false);
