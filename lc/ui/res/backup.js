// Reload button.
document.getElementById("a-backup-reload").onclick = function () {
    switch_to_tab("backup");
};

function hide_archive_modal(archive) {
    document.getElementById("archive-modal").classList.remove("is-active");
    show_spinner(false);
}

function show_archive_modal(archive, content) {
    document.getElementById("archive-modal-title").textContent = "Content of backup archive: " + archive;
    document.getElementById("archive-modal-content").innerHTML = content;
    document.getElementById("archive-modal-content").scrollTop = 0;
    document.getElementById("archive-modal").classList.add("is-active");
    document.getElementById("archive-modal-close").onclick = function () {
        show_spinner(true);
        fetch(url("/do/umount/" + archive))
        .then(response => response.text())
        .then(text => hide_archive_modal(archive));
    };
    show_spinner(false);
}

function mount_backup(archive) {
    show_spinner(true);
    fetch(url("/do/mount/" + archive))
    .then(response => response.text())
    .then(text => show_archive_modal(archive, text));
}
