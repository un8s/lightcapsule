# Copyright 2020 info@un8s.com
# License: MIT, see LICENSE for details.

# CSS Framework: https://bulma.io/
# Icons: https://ionicons.com/

# TODO Lazy mounting of backup archives: either mount in background while already showing cached listing, or mount only upon first download.
# TODO Pagination for large archive listings.

import bz2
import datetime
import json
import os
import sys
import time
import urllib.parse

import web

from lib import log, timestamp, uuid
from lib.srv import ServiceDef, Service

from disk import SDEF as SDISK
from light import SDEF as SLIGHT
from health import SDEF as SHEALTH
from backup import SDEF as SBACKUP

SDEF = ServiceDef({
    'app': 'lc',
    'name': 'ui',
    'version': '20.12',
    'description': 'HTTP User Interface service.',
    'checkuid': 0,
    'mbus': { 'broker.name': '_lc' },
    'conf': {
        'hd.mount': SDISK.conf_def('hd.mount')
    },
    'events': {
        'consume': {
            'on_health': {
                'source': SHEALTH,
                'filter': 'health\.update'
            },
            'on_disk_health': {
                'source': SDISK,
                'filter': '.*\.health'
            },
            'on_disk_state': {
                'source': SDISK,
                'filter': '.*\.state'
            },
            'on_hd_shares': {
                'source': SDISK,
                'filter': 'hd\.shares'
            },
            'on_light': {
                'source': SLIGHT
            },
            'on_alert': {
                'source': [SDISK, SHEALTH, SBACKUP],
                'filter': 'alert\..*'
            }
        },
        'emit': {
            'ui.startup': {
                'args': {
                    'version': { 'type': str }
                }
            },
            'ui.shutdown': {}
        }
    },
    'schedules': {
        'every_five_minutes': {
            'interval': 5
        }
    }
})

# Low-level configuration from environment.
_TEMPLATES = os.getenv('LC_UI_TEMPLATES', './lc/ui')
_SERVER_PORT = os.getenv('LC_UI_PORT', '80')

def _format_size(b):
    """Return human readable size from bytes b."""
    kb = b / 1024
    mb = kb / 1024
    gb = mb / 1024
    if gb >= 10.0:
        return '{:,} GB'.format(int(gb))
    elif gb >= 1.0:
        return '{:.1f} GB'.format(gb)
    elif mb >= 10.0:
        return '{:,} MB'.format(int(mb))
    elif mb >= 1.0:
        return '{:.1f} MB'.format(mb)
    elif kb >= 10.0:
        return '{:,} KB'.format(int(kb))
    return '{:.1f} KB'.format(kb)

def _format_temp(t):
    """Return temperature with unit."""
    return '-' if t is None else f'{t} °C'

def _qualify_load(l):
    """Return qualification of load depending on fixed thresholds."""
    if l >= 90.0:
        return 'is-danger'
    elif l >= 75.0:
        return 'is-warning'
    return 'is-success'

def _format_data(d):
    """Return event data."""
    if 'error' in d:
        return d['error']
    return ', '.join([f'{k}={v}' for k, v in d.items()])

_format_helpers = {
    'format_size': _format_size,
    'format_temp': _format_temp,
    'qualify_load': _qualify_load,
    'format_data': _format_data
}

_uis = None
_render = web.template.render(_TEMPLATES, globals=_format_helpers)
_health = {}
_alerts = {}


def _timestamp():
    """Return timestamp in minute precision."""
    return int(time.time() / 60)


class _Session:
    """Browser session."""

    def __init__(self, sid):
        self._data = {'sid': sid, 'sse': None}
        self.update()

    def update(self):
        """Update last access timestamp."""
        self._data['lru'] = _timestamp()

    def is_expired(self, max_age):
        """Return if session was accessed more than max_age minutes ago."""
        return _timestamp() - self._data['lru'] >= max_age

    def __getitem__(self, key):
        self.update()
        return self._data[key]

    def __setitem__(self, key, value):
        self.update()
        self._data[key] = value

    def get(self, key, default):
        self.update()
        return self._data.get(key, default)

    def __str__(self):
        return str(self._data)


class _SessionManager:
    """Browser session manager."""

    def __init__(self):
        self._session = {}

    def init(self, sid):
        """Return True if sid is known. Make sid known. Update LRU."""
        assert isinstance(sid, str) and len(sid), sid
        if sid in self._session:
            self._session[sid].update()
            log.debug(f'Existing session: {self._session[sid]}')
            return True
        else:
            self._session[sid] = _Session(sid)
            log.debug(f'New session: {self._session[sid]}')
            return False

    def get(self, sid):
        """Return session, or None."""
        return self._session.get(sid, None)

    def purge(self, max_age):
        """Purge expired sessions."""
        for sid in list(self._session.keys()):
            s = self._session[sid]
            if s.is_expired(max_age):
                log.debug(f'Purge expired session: {s}')
                s['sse'] = None
                del self._session[sid]
            else:
                log.debug(f'Session is still valid (age={_timestamp()-s._data["lru"]}): {s}')

_sm = _SessionManager()


class _HttpMain:
    """Return main page."""

    def GET(self, what):
        q = web.input(sid=None)
        sid = q.sid or uuid()
        known_session = _sm.init(sid)

        if what is None:
            return _render.main(SDEF.version, sid)

        if what == 'mini':
            return _render.mini(SDEF.version, sid)

        if not known_session:
            return '_reload'

        if what == 'backup':
            b = _uis.send_command(SBACKUP, 'list', 60.0, cache=True, info=True)
            if b and b.error() is None:
                return _render.backup(b['share']+b['timemachine'])
            else:
                return _render.backup([])

        if what == 'status':
            session = _sm.get(sid)
            if session is None:
                return '_reload'
            session['status.reload'] = True

        return getattr(_render, what)()


class _HttpResources:
    """Return resources."""

    def GET(self, path):
        if path.endswith('.js'):
            web.header('Content-type', 'text/javascript')
        elif path.endswith('.css'):
            web.header('Content-type', 'text/css')
        elif path.endswith('.svg'):
            web.header('Content-type', 'image/svg+xml')
        with open(f'{_TEMPLATES}{path}', 'rb') as f:
            content = f.read()
        return content


# https://www.html5rocks.com/en/tutorials/eventsource/basics/
# https://medium.com/conectric-networks/a-look-at-server-sent-events-54a77f8d6ff7
class _HttpSse:
    """Return SSE updates."""

    def __init__(self):
        self._sse_render = web.template.render(f'{_TEMPLATES}/sse', globals=_format_helpers)
        self._shown_alerts = None

    def render_sse(self, what, *args):
        """Return SSE formatted content of rendered page."""
        page = str(getattr(self._sse_render, what)(*args))
        data = ''
        for line in page.splitlines():
            if line:
                data += f'data: {line}\n'
        return data

    def GET(self, what):
        q = web.input(sid=None)
        assert q.sid is not None
        session = _sm.get(q.sid)
        if session is None:
            return '_reload'
        session['sse'] = self
        web.header('Content-Type', 'text/event-stream')
        web.header('Cache-Control', 'no-cache')
        # TODO Only yield if data has changed.
        tick = 300
        while True:
            status_reload = session.get('status.reload', False)
            session['status.reload'] = False

            if tick % 300 == 0 or session.get('log.reload', False) or status_reload:
                session['log.reload'] = False
                yield f'event: log\n{self.render_sse("log", _uis.read_log(n=-50, reverse=True))}\n'

            if tick % 60 == 0 or status_reload:
                yield f'event: disks\n{self.render_sse("disks", _health)}\n'

            if tick % 10 == 0 or status_reload:
                yield f'event: system\n{self.render_sse("system", _health)}\n'

            if tick % 1 == 0:
                d = {
                    'lc.cpu.load': _health.get('lc.cpu.load', '-'),
                    'lc.cpu.temp': _format_temp(_health.get('lc.cpu.temp')),
                    'bc.cpu.load': _health.get('bc.cpu.load', '-'),
                    'bc.cpu.temp': _format_temp(_health.get('bc.cpu.temp')),
                    'lc.time': datetime.datetime.now().isoformat(sep=" ", timespec="minutes"),
                    'hd.state': 'on' if _health.get('hd.started', False) else 'off',
                    'hd.access': 'true' if _health.get('hd.write_access', None) is not None else 'false',
                    'hd.share': 'true' if 'share' in _health.get('hd.shares', {}) else 'false',
                    'hd.timemachine': 'true' if 'timemachine' in _health.get('hd.shares', {}) else 'false',
                    'bk.online': 'true' if _health['bc.online'] else 'false',
                    'bk.state': 'on' if _health.get('bk.started', False) else 'off',
                    'bk.access': 'true' if _health.get('bk.write_access', None) is not None else 'false',
                    'light.state': 'on' if _health.get('light.state', {'on': False})['on'] is True else 'off' 
                }
                yield f'event: status\ndata: {json.dumps(d)}\n\n'

            if tick % 1 == 0:
                if _alerts != self._shown_alerts:
                    self._shown_alerts = dict(_alerts)
                    data = '\n'.join([str(al) for al in self._shown_alerts.values()])
                    yield f'event: alerts\ndata: {data}\n\n'

            tick -= 1
            if tick == 0:
                tick = 300
            time.sleep(1.0)


class _HttpActions:
    """Process commands."""

    @staticmethod
    def _format_ls(share, lsr, sid):
        """Return parsed ls output."""
        r = []
        mount_name, dir_name = _uis.conf['hd.mount'], None
        for line in lsr.splitlines():
            if len(line.strip()) == 0:
                continue
            typ, size, date, time, name = line.split(maxsplit=4)
            if typ == 'd':
                if dir_name is not None:
                    r.append('&nbsp;&nbsp;')
                dir_name = name
                r.append(f'&nbsp;&nbsp;<b>{dir_name}</b>')
            else:
                r.append(f'&nbsp;&nbsp;{date} {time} {size} <a class="has-text-black" target="_blank" href="/do/download/{urllib.parse.quote("/".join([mount_name, share, dir_name, name]))}?sid={sid}">{name}</a>')
        return '<br>'.join(r)

    def GET(self, a1, a2):
        q = web.input(sid=None)
        log.info(f'Do {a1} {a2} for {q.sid}')
        assert q.sid is not None
        session = _sm.get(q.sid)
        if session is None:
            return '_reload'

        if a1 in ('halt', 'reboot'):
            assert a2 in ('now',), a2
            if a2 == 'now':
                _uis.send_command(SHEALTH, 'shutdown', who='*', what=a1, when=a2)
        
        if a1 in ('light',):
            assert a2 in ('toggle', 'up', 'down'), a2
            if a2 == 'toggle':
                _uis.send_command(SLIGHT, 'light', switch=a2)
            elif 'light.state' in _health:
                step = 10 if a2 == 'up' else -10
                _uis.send_command(SLIGHT, 'light', brightness=max(1, min(100, _health['light.state']['brightness'] + step)))

        if a1 in ('power',):
            assert a2 in ('hd', 'bk'), a2
            _uis.send_command(SDISK, 'toggle', name=a2, mode='ondemand')

        if a1 in ('reload',):
            assert a2 in ('log',), a2
            session['log.reload'] = True

        if a1 in ('mount', 'umount'):
            assert len(a2), a2
            ma = _uis.send_command(SBACKUP, a1, 5*60.0, archive=a2)
            if a1 == 'mount' and ma and ma.error() is None:
                if ma['mounted']:
                    return _HttpActions._format_ls(ma['share'], bz2.decompress(ma['ls-bz2']).decode(encoding='utf-8'), q.sid)
                else:
                    return f'Unable to mount archive: {a2}'

        if a1 in ('download',):
            assert len(a2), a2
            try:
                with open(urllib.parse.unquote(a2), 'rb') as f:
                    content = f.read()
                return content
            except Exception as ex:
                log.debug(ex)

        return f'{a1} {a2}'


class _UiService(Service):
    """HTTP user interface service."""

    def __init__(self):
        super().__init__(SDEF)

    def every_five_minutes(self):
        """Purge expired sessions."""
        _sm.purge(max_age=15)
        _health['bc.online'] = (timestamp() - _health.get('bc._update', 0)) < 30*1000*1000

    def on_health(self, e):
        """Update current overall health."""
        assert e.matches('health.update'), e
        if e.error() is None:
            _health.update(e.data())
            if 'bc.cpu.count' in e:
                _health['bc._update'] = e.timestamp

    def on_disk_health(self, e):
        """Update current disk health."""
        assert e.matches('.*\.health'), e
        if e.error() is None:
            _health.update(e.data())

    def on_disk_state(self, e):
        """Update current disk state."""
        assert e.matches('.*\.state'), e
        if e.error() is None:
            _health[f'{e.name}.started'] = e.started

    def on_hd_shares(self, e):
        """Update shares info."""
        assert e.matches('hd.shares'), e
        if e.error() is None:
            _health['hd.shares'] = e['shares']

    def on_light(self, e):
        """Handle light state."""
        assert e.matches('state'), e
        if e.error() is None:
            _health['light.state'] = e.data()

    def on_alert(self, e):
        """Handle alerts."""
        assert e.matches('alert\..*'), e
        alert = e.tags('alert\..*')
        if alert:
            akey = alert[0][len('alert.'):]
            if akey in e:
                aval = e[akey]
                if aval is not None:
                    _alerts[akey] = f'{akey} {aval}'
                else:
                    _alerts.pop(akey, None)

    def _start(self, argv=None):
        self.emit_event('ui.startup', {'version': SDEF.version}, log=True)

        # Init health info.
        h = self.send_command(SHEALTH, 'health')
        if h and h.error() is None:
            _health.update(h.data())
            if 'bc.cpu.count' in h:
                _health['bc._update'] = h.timestamp
        _health['bc.online'] = 'bc._update' in _health
        h = self.send_command(SDISK, 'health')
        if h and h.error() is None:
            for dh in h.data().values():
                _health.update(dh)
        h = self.send_command(SLIGHT, 'state')
        if h and h.error() is None:
            _health['light.state'] = h.data()

    def _run(self):
        """Start web server."""
        app = web.application((
            '/(status|backup|settings|mini)?', '_HttpMain',
            '(/res/.*)', '_HttpResources',
            '/sse(.*)', '_HttpSse',
            '/do/(.*?)/(.*)', '_HttpActions'
        ), globals())
        web.httpserver.runsimple(app.wsgifunc(), ('0.0.0.0', int(_SERVER_PORT)))

    def _stop(self):
        self.emit_event('ui.shutdown', log=True)
        # This low level exit is required to stop the web server.
        log.info(f'{self._sd.name} service stopped.')
        os._exit(os.EX_OK)


if __name__ == '__main__':
    web.config.debug = True
    _uis = _UiService()
    _uis.start(sys.argv)
