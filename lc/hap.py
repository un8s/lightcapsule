# Copyright 2020 info@un8s.com
# License: MIT, see LICENSE for details.

import os
import os.path
import signal
import sys

import pyhap.accessory
import pyhap.accessory_driver
import pyhap.const

from lib import log, sys_info
from lib.srv import ServiceDef, Service

from disk import SDEF as SDISK
from light import SDEF as SLIGHT


def _check_hap_pin(value):
    if re.match('\d{3}-\d{2}-\d{3}$', value) is None:
        raise ValueError(f"Value for 'hap-pin' must be of format '000-00-000': {value}")
    return value

SDEF = ServiceDef({
    'app': 'lc',
    'name': 'hap',
    'version': '20.12',
    'description': 'HAP service.',
    'checkuid': 0,
    'mbus': { 'broker.name': '_lc' },
    'conf': {
        'hap.pin': {
            'value': '862-62-484',
            'type': str,
            'check': _check_hap_pin,
            'required': True
        }
    },
    'events': {
        'consume': {
            'on_disk_state_changed': {
                'source': SDISK,
                'filter': 'hd\.state'
            },
            'on_light_state_changed': {
                'source': SLIGHT
            }
        }
    }
})


LIB = os.getenv('LC_LIB', '/tmp')   # Set to something persistent. Otherwise user's need to reconnect.
PORT = os.getenv('LC_HAP_PORT', '51826')



class _LCAccessory(pyhap.accessory.Accessory):
    """LightCapsule accessory."""

    category = pyhap.const.CATEGORY_SWITCH

    def __init__(self, driver, srv):
        super().__init__(driver, 'Shares')
        self._srv = srv
        si = sys_info()
        self.set_info_service(SDEF.version, 'un8s.com', 'LightCapsule', si['sys.serial'])
        
        service = self.add_preload_service('Switch', chars=['On'])
        self.shares_switch = service.configure_char('On', setter_callback=self._set_disk_on)

        service = self.add_preload_service('Lightbulb', chars=['On', 'Brightness'])
        self.light_switch = service.configure_char('On', setter_callback=self._set_light_on)
        self.light_brightness = service.configure_char('Brightness', setter_callback=self._set_light_brightness)

    def _set_disk_on(self, value):
        """This setter is called by the HAP server."""
        log.debug(f'Set disk: {value}')
        h = self._srv.send_command(SDISK, 'start' if value else 'stop', name='hd', mode='ondemand')
        self.shares_switch.set_value(h is not None and h['hd.started'])

    def _set_light_on(self, value):
        """This setter is called by the HAP server."""
        log.debug(f'Set light: {value}')
        h = self._srv.send_command(SLIGHT, 'light', switch='on' if value else 'off')
        self.light_switch.set_value(h is not None and h['on'])

    def _set_light_brightness(self, value):
        """This setter is called by the HAP server."""
        log.debug(f'Set light brightness: {value}')
        h = self._srv.send_command(SLIGHT, 'light', brightness=value)
        self.light_brightness.set_value(value)


class _HAPService(Service):
    """HAP service."""

    def __init__(self):
        super().__init__(SDEF)
        self._driver = None

    def on_disk_state_changed(self, e):
        assert e.matches('hd.state'), e
        if e.error() is None:
            self._lca.shares_switch.set_value(e.started)

    def on_light_state_changed(self, e):
        if e.error() is None:
            self._lca.light_switch.set_value(e.on)
            self._lca.light_brightness.set_value(e.brightness)

    def _start(self, argv):
        # Create library directory if necessary.
        if not os.path.exists(LIB):
            os.mkdir(LIB)

        self._driver = pyhap.accessory_driver.AccessoryDriver(port=int(PORT), pincode=self.conf['hap.pin'].encode('utf-8'), persist_file=f'{LIB}/hap.state')
        signal.signal(signal.SIGTERM, self._driver.signal_handler)
        signal.signal(signal.SIGINT, self._driver.signal_handler)

        self._lca = _LCAccessory(self._driver, self)
        self._driver.add_accessory(accessory=self._lca)

        # Initialize with current state.
        h = self.send_command(SDISK, 'health', name='hd')
        self._lca.shares_switch.set_value(h and 'hd.started' in h and h['hd.started'])
        h = self.send_command(SLIGHT, 'state')
        if h and h.error() is None:
            self._lca.light_switch.set_value(h.on)
            self._lca.light_brightness.set_value(h.brightness)

    def _run(self):
        # Start HAP server.
        self._driver.start()


if __name__ == '__main__':
    _HAPService().start(sys.argv)
