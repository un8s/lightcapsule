# Copyright 2020 info@un8s.com
# License: MIT, see LICENSE for details.

# TODO Collect usage stats.

import enum
import socket
import subprocess
import sys
import time

import psutil

from lib import log
from lib.srv import ServiceDef, Service

from disk import SDEF as SDISK


SDEF = ServiceDef({
    'app': 'lc',
    'name': 'health',
    'version': '20.12',
    'description': 'Overall health monitoring service.',
    'checkuid': 0,
    'mbus': { 'broker.name': '_lc' },
    'commands': {
        'health': {},
        'shutdown': {
            'args': {
                'who': { 'type': str, 'required': True, 'choices': ['lc', 'bc', '*'] },
                'what': { 'type': str, 'required': True, 'choices': ['reboot', 'halt'] },
                'when': { 'type': str, 'required': True, 'choices': ['now'] }
            }
        }
    },
    'events': {
        'consume': {
            'on_disk_event': {
                'source': SDISK
            }
        },
        'emit': {
            'health.update': {
                'args': {}
            },
            'proc.missing': {
                'args': {
                    'proc.missing': { 'type': str }
                }
            },
            'system.shutdown': {
                'args': {
                    'who': { 'type': str, 'choices': ['lc', 'bc'] },
                    'what': { 'type': str, 'choices': ['reboot', 'halt'] },
                    'when': { 'type': str, 'choices': ['now'] }
                }
            }
        }
    },
    'schedules': {
        'every_15_minutes': {
            'interval': 15,
            'init': True
        },
        'every_minute': {
            'interval': 1,
            'init': True
        },
        'every_five_seconds': {
            'interval': 0.05,
            'init': True
        }
    }
})


class _SysType(enum.Enum):
    LightCapsule = 'lc'
    BackupCompanion = 'bc'


class _HealthService(Service):
    """Health monitoring service."""

    def __init__(self):
        super().__init__(SDEF)
        self._disk_health = {}  # Disk health is only collected for alerting, not for relaying.
        self._health, self._published_health = {}, {}

    def prefixed_health(self):
        """Return health data with each key prefixed."""
        return {f'{self._type.value}.{k}': v for k, v in self._health.items()}

    def every_five_seconds(self):
        """Executed once every five seconds."""
        # CPU
        temp = psutil.sensors_temperatures()
        temp = temp.get('cpu_thermal') or temp.get('soc_thermal')
        self._health['cpu.temp'] = int(temp[0].current)

        # Publish health.
        if self._published_health != self._health:
            self.emit_event('health.update', self.prefixed_health())

            # TODO Alerting: cpu.load, cpu.temp, hd.load, bk.load, sys.load

            # Processes
            if 'proc.missing' in self._health:
                if 'proc.missing' not in self._published_health or self._health['proc.missing'] != self._published_health['proc.missing']:
                    self.emit_event('proc.missing', {'proc.missing': self._health["proc.missing"] if len(self._health["proc.missing"]) else None}, alert=True)

            self._published_health = dict(self._health)

    def every_minute(self):
        """Executed once per minute."""
        # Memory
        vm = psutil.virtual_memory()
        self._health['mem.total'] = vm.total
        self._health['mem.used'] = vm.used
        self._health['mem.free'] = vm.free
        self._health['mem.load'] = vm.percent
        swap = psutil.swap_memory()
        self._health['swap.total'] = swap.total
        self._health['swap.used'] = swap.used
        self._health['swap.free'] = swap.free
        self._health['swap.load'] = swap.percent
        
        # Network
        netc = psutil.net_io_counters(pernic=True)
        self._health['lan.errors'] = None if 'eth0' not in netc else netc['eth0'].errin + netc['eth0'].errout + netc['eth0'].dropin + netc['eth0'].dropout
        self._health['wlan.errors'] = None if 'wlan0' not in netc else netc['wlan0'].errin + netc['wlan0'].errout + netc['wlan0'].dropin + netc['wlan0'].dropout

        # Processes
        PROCS = (
            'disk.py', 'light.py', 'health.py', 'backup.py', 'ui.py',
            'redis-server', 'smbd', 'nmbd'
        ) if self._type == _SysType.LightCapsule else {
            'disk.py', 'health.py'
        }
        procs = {}
        for proc in psutil.process_iter(['pid', 'cmdline', 'status']):
            cmd = ' '.join(proc.info['cmdline']).strip()
            if cmd == '':
                continue
            for p in PROCS:
                if p in cmd:
                    procs[p] = proc.info
        missing = []
        for p in PROCS:
            if p not in procs or procs[p]['status'] not in ('running', 'sleeping'):
                missing.append(p)
        if 'smbd' in missing and not self._health.get('hd.started', False):
            missing.remove('smbd')
        self._health['proc.missing'] = missing

    def every_15_minutes(self):
        """Executed once every 15 minutes."""
        # CPU
        self._health['cpu.count'] = psutil.cpu_count()
        self._health['cpu.load'] = round([x / psutil.cpu_count() * 100 for x in psutil.getloadavg()][-1], 1)

        # Network
        self._health['host.name'] = socket.gethostname()
        
        ni = psutil.net_if_addrs()

        nic = False
        for na in ni.get('eth0', []):
            if na.family == socket.AF_INET:
                self._health['lan.ip4'] = na.address
                nic = True
            elif na.family == socket.AF_INET6:
                self._health['lan.ip6'] = na.address
                nic = True
            elif na.family == socket.AF_PACKET:
                self._health['lan.mac'] = na.address
        if not nic:
            self._health.pop('lan.ip4', None)
            self._health.pop('lan.ip6', None)
            self._health.pop('lan.mac', None)
        else:
            self._health['lan.speed'] = psutil.net_if_stats()['eth0'].speed
        nic = False
        for na in ni.get('wlan0', []):
            if na.family == socket.AF_INET:
                self._health['wlan.ip4'] = na.address
                nic = True
            elif na.family == socket.AF_INET6:
                self._health['wlan.ip6'] = na.address
                nic = True
            elif na.family == socket.AF_PACKET:
                self._health['wlan.mac'] = na.address
        if not nic:
            self._health.pop('wlan.ip4', None)
            self._health.pop('wlan.ip6', None)
            self._health.pop('wlan.mac', None)

    def on_disk_event(self, e):
        if e.error() is not None:
            return
        if e.matches('.*\.access'):
            self._disk_health[f'{e.name}.write_access'] = e.write_access
        elif e.matches('.*\.state'):
            self._disk_health[f'{e.name}.started'] = e.started
        elif e.matches('.*\.health'):
            self._disk_health.update(e.data())
        elif e.matches('hd.shares'):
            self._disk_health['hd.shares'] = e['shares']

    def do_health(self):
        # TODO Optional filter for specific health metrics?
        return self.response(**self.prefixed_health())

    def do_shutdown(self, who, what, when):
        if who in ('*', self._type.value):
            # Trigger shutdown with a delay so that we can give some feedback first.
            self.emit_event('system.shutdown', {'who': self._type.value, 'what': what, 'when': when}, log=True)
            time.sleep(3)
            subprocess.run(f'/sbin/shutdown {"-r" if what == "reboot" else "-h"} {when}', shell=True)

    def _start(self, argv):
        """Running as LightCapsule or BackupCompanion."""

        if len(argv) > 1 and argv[1] == '--lc':
            self._type = _SysType.LightCapsule

        elif len(argv) > 1 and argv[1] == '--bc':
            self._type = _SysType.BackupCompanion

        else:
            sys.exit(f'usage: {argv[0]} --lc|--bc')
            
        # Initialize disk health.
        h = self.send_command(SDISK, 'health')
        if h and h.error() is None:
            for disk_health in h.data().values():
                self._disk_health.update(disk_health)


if __name__ == '__main__':
    _HealthService().start(sys.argv)
