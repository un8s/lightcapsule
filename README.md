_LightCapsule_ is a simple NAS appliance that offers a public share for the Intranet and also serves as backup destination for Time Machine. Its _BackupCompanion_ is a second NAS appliance that serves as pure backup server (i.e. primary backup of the public share and secondary backup of Time Machine backup.)

The idea was born when my Seagate BlackArmor NAS with two HDD in RAID-1 died. To rescue the data on the disks, I needed a Linux machine to mount the disks with the appropriate RAID/LVM commands. As one of the cheaper ways to build a Linux machine is chose a Raspberry PI, and the next logical step was then to build a new NAS based on the PI.

I decided against RAID and to use the second HDD instead as backup disk for the NAS disk. First, I wanted to add both disks to the same PI, but then I came up with the more flexible solution of splitting the "NAS" into the _LightCapsule_ and its _BackupCompanion_.

The solution is meant as no-frills, no-fuss storage shared by multiple users with different devices and OS (iOS, macOS, Windows) in a private and trusted Intranet, with no need to open access to the NAS from the Internet.

## Features
- Disk share for Windows, Mac, etc (one public share, no users)
- Backup destination for Time Machine
- Integrated, automatic backups to the _BackupCompanion_
- LED matrix as ambient light and display of status information (disk access, disk usage, etc.)
- Controlled disk power:
    - manual
    - scheduled = useful for Time Machine backup windows
    - automatic power off when idle
- [HomeKit enabled]((https://gitlab.com/un8s/lightcapsule/-/wikis/Screenshots)) (ambient light on/off, disk power on/off)
- Simple [HTTP User Interface](https://gitlab.com/un8s/lightcapsule/-/wikis/Screenshots)
    - Monitor: disk usage, health check
    - Configuration:
        - Enable / Disable: share, timemachine
        - Exclusion list for automatic backup
    - Backup / Restore

## Hardware
see [Hardware](https://gitlab.com/un8s/lightcapsule/-/wikis/Hardware)

## Software
see [Software](https://gitlab.com/un8s/lightcapsule/-/wikis/Software)

## License
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
